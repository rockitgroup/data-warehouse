class Constants:

    MYSQL_HOST = 'MYSQL_HOST'
    MYSQL_USER = 'MYSQL_USER'
    MYSQL_PASSWORD = 'MYSQL_PASSWORD'
    MYSQL_DB_NAME = 'MYSQL_DB_NAME'

    def __init__(self):
        pass
