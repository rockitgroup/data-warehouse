import logging.config
import configparser
import os

from constants import Constants

GLOBAL_SECTION = "Global"

class Config:

    def __init__(self, config_file):
        self.config = configparser.ConfigParser()
        self.config._interpolation = configparser.ExtendedInterpolation()
        self.config.read(config_file)

    def get_config(self):
        return self.config

    def get_global_config_value(self, key):
        return self.config.get(GLOBAL_SECTION, key)

    def get_config_value(self, section, key):
        return self.config.get(section, key)

    def get_mysql_host(self):
        return self.config.get(GLOBAL_SECTION, Constants.MYSQL_HOST)

    def get_mysql_user(self):
        return self.config.get(GLOBAL_SECTION, Constants.MYSQL_USER)

    def get_mysql_password(self):
        return self.config.get(GLOBAL_SECTION, Constants.MYSQL_PASSWORD)

    def get_mysql_db_name(self):
        return self.config.get(GLOBAL_SECTION, Constants.MYSQL_DB_NAME)

    def get_logger(self, file_name, console_logging=True):
        logger_name = ''
        if console_logging:
            logger_name = 'console'

        log_conf_file = self.config.get(GLOBAL_SECTION, "LOGGING_CONFIG_FILE")
        log_file_name = os.path.join(self.config.get(GLOBAL_SECTION, "LOG_FOLDER"), file_name)
        logging.config.fileConfig(log_conf_file, defaults={'logfilename': log_file_name})
        return logging.getLogger(logger_name)



