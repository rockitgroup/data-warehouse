import sys, os

sys.path.insert(0, os.path.abspath(os.path.join(os.getcwd(), "../../common")))

import argparse
import datetime
import pandas as pd

import mysql.connector

from settings import Config

from bs4 import BeautifulSoup

MINIMUM_CHARACTER = 10

UNPARSABLE_TAG = ['html', 'body']

class AnalyzeOriginalHtmlContent:

    def __init__(self, args):
        self.config_file = args.config_file

        config = Config(self.config_file)

        self.db_config = { "host": config.get_mysql_host(),
                           "user": config.get_mysql_user(),
                           "password": config.get_mysql_password(),
                           "database": config.get_mysql_db_name() }

        self.logger = config.get_logger("crawl_app_store_data.log", True)


    def process(self):
        db_con = mysql.connector.connect(**self.db_config)
        query = "SELECT id, originHtmlContent FROM article_crawling_results WHERE status='ACTIVE' AND originHtmlContent IS NOT NULL"

        crawl_results = pd.read_sql(query, db_con)

        for index, row in crawl_results.iterrows():
            soup = BeautifulSoup(row['originHtmlContent'], 'html5lib') 

            for item in soup.find_all("a"):
                item.name = "span"

            for item in soup.find_all("button"):
                item.decompose() 
                
            for item in soup.find_all("figcaption"):
                item.decompose() 

            for item in soup.find_all():
                if len(item.get_text(strip=True)) == 0:
                    item.extract()

            cursor = db_con.cursor(buffered=True)

            update_query = "UPDATE article_crawling_results SET status='HTML_PROCESSED', updatedAt=NOW(), processedHtmlContent=%(htmlContent)s WHERE id=%(id)s"
            cursor.execute(update_query, { "htmlContent": soup.prettify().encode("utf-8"), "id": row['id']})
            
            db_con.commit()
        
        db_con.close()


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", dest="config_file", help="Configuration File", required=True)
    args = parser.parse_args(argv)

    app = AnalyzeOriginalHtmlContent(args)
    app.process()


if __name__ == '__main__':
    main(sys.argv[1:])

