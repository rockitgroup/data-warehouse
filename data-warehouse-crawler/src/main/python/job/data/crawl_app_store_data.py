import sys
import os
import argparse
import datetime

from settings import CRAWL_APP_LOGIN_PAGE_URL
from settings import CRAWL_APP_TOP_CHART_PAGE_TEMPLATE_URL_MAP
from settings import CRAWL_APP_SORT_TYPES, APP_RANKING_TYPES
from settings import CRAWL_APP_COUNTRY_CODES
from settings import Config

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from bs4 import BeautifulSoup

class AppStoreCrawler:

    def __init__(self, args):
        self.config_file = args.config_file

        sys.path.insert(0, os.path.abspath(os.path.join(self.config_file, "../../common")))

        config = Config(self.config_file)

        self.email = args.email
        self.password = args.password

        self.chrome_driver = config.get_global_config_value("CHROME_DRIVER_FILE")
        self.logger = config.get_logger("crawl_app_store_data.log", True)

    def process(self):
        driver = webdriver.Chrome(self.chrome_driver)
        driver.get(CRAWL_APP_LOGIN_PAGE_URL)

        driver.implicitly_wait(5)

        email_element = driver.find_element_by_id("email")
        email_element.clear()
        email_element.send_keys(self.email)

        password_element = driver.find_element_by_id("password")
        password_element.clear()
        password_element.send_keys(self.password)

        submit_element = driver.find_element_by_id("submit")
        submit_element.send_keys(Keys.RETURN)

        WebDriverWait(driver, 15).until(
            EC.presence_of_element_located((By.CLASS_NAME, "user-email"))
        )

        for app_platform, app_platform_template_url in CRAWL_APP_TOP_CHART_PAGE_TEMPLATE_URL_MAP.iteritems():
            for rank_type in APP_RANKING_TYPES:
                for sort_type in CRAWL_APP_SORT_TYPES:
                    for country_code in CRAWL_APP_COUNTRY_CODES:
                        params = {
                            "countryCode": country_code,
                            "rankType": rank_type,
                            "sortType": sort_type,
                            "date": str(datetime.date.today())
                        }

                        dashboard_url = app_platform_template_url % params

                        driver.get(dashboard_url)

                        dashboard_table = WebDriverWait(driver, 15).until(
                            EC.presence_of_element_located((By.CLASS_NAME, "dashboard-table"))
                        )

                        page = BeautifulSoup(dashboard_table.get_attribute('innerHTML'), "html5lib")

                        rows = page.find_all("tr", {'class': 'main-row'})

                        for row in rows:
                            print row

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", dest="config_file", help="Configuration File", required=True)
    parser.add_argument("-e", dest="email", help="Enter email", required=True)
    parser.add_argument("-p", dest="password", help="Enter password", required=False, default=None)
    args = parser.parse_args(argv)

    crawler = AppStoreCrawler(args)
    crawler.process()


if __name__ == '__main__':
    main(sys.argv[1:])

