package com.rockitgroup.infrastructure.data.warehouse.crawler.service.impl;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleDetailUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleListingUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleSiteCrawlingProperty;
import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.service.*;
import com.rockitgroup.infrastructure.data.warehouse.common.util.RestApiUtils;
import com.rockitgroup.infrastructure.data.warehouse.crawler.Constants;
import com.rockitgroup.infrastructure.data.warehouse.crawler.service.PageExtractor;
import com.rockitgroup.infrastructure.data.warehouse.crawler.service.worker.MediumDetailPageCrawlerWorker;
import com.rockitgroup.infrastructure.data.warehouse.crawler.util.WebDriverUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Component
public class MediumPageExtractorImpl implements PageExtractor {

    private static final Integer MAX_PARENT_LEVEL = 5;

    private static final Integer SCROLL_DOWN_MAX_HEIGHT = 3000;
    private static final Integer SCROLL_DOWN_HEIGHT_SPEED = 200;

    private static final Integer WAITING_TIME_MILLISECONDS = 2000;

    private static final String TOPIC_PAGE_URL_PROPERTY_NAME = "TOPIC_PAGE_URL";
    private static final String EMAIL_PROPERTY_NAME = "EMAIL";
    private static final String PASSWORD_PROPERTY_NAME = "PASSWORD";

    @Autowired
    @Qualifier("medium.crawlDetailPage.secretKey")
    private String secretKey;

    @Autowired
    @Qualifier("medium.crawlDetailPage.numThread")
    private Integer numThread;

    @Autowired
    @Qualifier("medium.crawlDetailPage.maxCrawlItem")
    private Integer maxCrawlItem;

    @Resource
    private SecurityService securityService;

    @Resource
    private ArticleListingUrlService articleListingUrlService;

    @Resource
    private CrawlingEventService crawlingEventService;

    @Resource
    private ArticleDetailUrlService articleDetailUrlService;

    @Resource
    private ArticleSiteCrawlingPropertyService articleSiteCrawlingPropertyService;

    @Resource
    private ArticleCrawlingResultService articleCrawlingResultService;

    private ExecutorService crawlDetailExecutorService;

    @PostConstruct
    public void postConstruct() {
        crawlDetailExecutorService = Executors.newFixedThreadPool(numThread, new ThreadFactoryBuilder().setNameFormat("crawl-detail-page-thread-pool").build());
    }

    @Override
    public void findListingPageUrlsFromHomePage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking) {
        Stack<CrawlingEvent> crawlingEvents = new Stack<>();

        Set<String> listingUrls = new HashSet<>();

        WebDriverUtils.getPage(driver, originSite.getHomeUrl(), crawlingEvents, CrawlTypeEnum.ARTICLE_HOME_PAGE);
        WebDriverUtils.waitForElement(driver, By.cssSelector("div.js-homeStream"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

        selectListingUrl(listingUrls, driver.findElements(By.cssSelector("a[href*='.com/topic/']")));
        selectListingUrl(listingUrls, driver.findElements(By.cssSelector("a[href*='.com/@']")));

        ArticleSiteCrawlingProperty topicPageUrlProperty = articleSiteCrawlingPropertyService.findPropertyBySiteAndName(originSite, TOPIC_PAGE_URL_PROPERTY_NAME);
        if (topicPageUrlProperty != null) {
            WebDriverUtils.getPage(driver, topicPageUrlProperty.getValue(), crawlingEvents, CrawlTypeEnum.ARTICLE_HOME_PAGE);
            WebDriverUtils.waitForElement(driver, By.cssSelector("div.js-sourceStream"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

            selectListingUrl(listingUrls, driver.findElements(By.cssSelector("a[href*='.com/topic/']")));
        } else {
            log.warn("There is no topic page url setting for Medium page");
        }

        saveListingUrls(listingUrls, originSite, batchJobTracking);

        saveCrawlingEvent(crawlingEvents);
    }

    @Override
    public void findDetailPageUrlsFromHomePage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking) {
        Stack<CrawlingEvent> crawlingEvents = new Stack<>();
        WebDriverUtils.getPage(driver, originSite.getHomeUrl(), crawlingEvents, CrawlTypeEnum.ARTICLE_HOME_PAGE);
        WebDriverUtils.waitForElement(driver, By.cssSelector("div.js-homeStream"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

        Map<String, String> detailUrlPostIdMap = new HashMap<>();

        int height = 0;
        while (height < SCROLL_DOWN_MAX_HEIGHT) {
            height += SCROLL_DOWN_HEIGHT_SPEED;

            JavascriptExecutor jse = (JavascriptExecutor )driver;
            jse.executeScript("window.scrollTo(0," + height + ");");

            List<WebElement> links = driver.findElements(By.cssSelector("a[data-post-id]"));

            for (WebElement link : links) {
                extractDetailUrl(detailUrlPostIdMap, link);
            }

            try {
                Thread.sleep(WAITING_TIME_MILLISECONDS);
            } catch (InterruptedException e) {
                log.error("Failed to sleep", e);
            }
        }

        saveDetailUrls(detailUrlPostIdMap, originSite, batchJobTracking);

        saveCrawlingEvent(crawlingEvents);
    }

    @Override
    public void findDetailPageUrlsFromListingPage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking) {
        List<ArticleListingUrl> articleListingUrls = articleListingUrlService.findAllSiteActiveUrls(originSite);

        Stack<CrawlingEvent> crawlingEvents = new Stack<>();

        for (ArticleListingUrl articleListingUrl : articleListingUrls) {
            WebDriverUtils.getPage(driver, articleListingUrl.getUrl(), crawlingEvents, CrawlTypeEnum.ARTICLE_LISTING_PAGE);

            Map<String, String> detailUrlPostIdMap = new HashMap<>();

            int height = 0;
            while (height < SCROLL_DOWN_MAX_HEIGHT) {
                height += SCROLL_DOWN_HEIGHT_SPEED;

                JavascriptExecutor jse = (JavascriptExecutor )driver;
                jse.executeScript("window.scrollTo(0," + height + ");");

                List<WebElement> links = driver.findElements(By.cssSelector("h3 a"));
                links.addAll(driver.findElements(By.cssSelector("h4 a")));

                for (WebElement link : links) {
                    extractDetailUrl(detailUrlPostIdMap, link);
                }

                List<WebElement> figureLinks = driver.findElements(By.cssSelector("figure"));
                for (WebElement figureLink : figureLinks) {
                    WebElement currentElement = figureLink;
                    int currentLevel = 0;
                    while (currentLevel < MAX_PARENT_LEVEL) {
                        currentElement = (WebElement) (jse.executeScript("return arguments[0].parentNode;", currentElement));
                        if (currentElement.getTagName().toLowerCase().equals("a")) {
                            extractDetailUrl(detailUrlPostIdMap, currentElement);
                            break;
                        }
                        currentLevel++;
                    }
                }

                try {
                    Thread.sleep(WAITING_TIME_MILLISECONDS);
                } catch (InterruptedException e) {
                    log.error("Failed to sleep", e);
                }
            }

            saveDetailUrls(detailUrlPostIdMap, originSite, batchJobTracking);

            CrawlingEvent crawlingEvent = (CrawlingEvent) crawlingEventService.save(CrawlingEvent.complete(crawlingEvents.pop(), TrackingEventResultEnum.SUCCESS));

            articleListingUrl.setLastCrawlingEvent(crawlingEvent);

            articleListingUrlService.save(articleListingUrl);
        }

        saveCrawlingEvent(crawlingEvents);
    }

    private void extractDetailUrl(Map<String, String> detailUrlPostIdMap, WebElement link) {
        String originalUrl = link.getAttribute("href");
        if (StringUtils.isNotEmpty(originalUrl) && originalUrl.contains("-")) {
            String filterUrl = (originalUrl.contains("?")) ? originalUrl.substring(0, originalUrl.indexOf("?")) : originalUrl;
            String originPostId = retrieveOriginPostIdFromUrl(filterUrl);
            detailUrlPostIdMap.put(filterUrl, originPostId);
        }
    }

    private String retrieveOriginPostIdFromUrl(String url) {
        String endDomainWord = "medium.com/";
        String originPostId = url.substring(url.lastIndexOf("-") + 1);
        String checkUrl = url.substring(0, url.indexOf(endDomainWord) + endDomainWord.length() + 1) + originPostId;
        log.info("Check Url is: {}", checkUrl);

        RestTemplate restTemplate = RestApiUtils.getRestTemplate();
        ResponseEntity responseEntity = restTemplate.getForEntity(checkUrl, String.class);
        return (responseEntity.getStatusCode() == HttpStatus.OK) ? originPostId : null;

    }

    @Override
    public void crawlDetailPage(OriginSite originSite, BatchJobTracking batchJobTracking) {
        List<ArticleDetailUrl> articleDetailUrls = articleDetailUrlService.findAllSiteActiveUrls(originSite, maxCrawlItem);

        ArticleSiteCrawlingProperty encryptedEmailProperty = articleSiteCrawlingPropertyService.findPropertyBySiteAndName(originSite, EMAIL_PROPERTY_NAME);
        ArticleSiteCrawlingProperty encryptedPasswordProperty = articleSiteCrawlingPropertyService.findPropertyBySiteAndName(originSite, PASSWORD_PROPERTY_NAME);

        try {
            String email = new String(securityService.decrypt(secretKey, Base64.getDecoder().decode(encryptedEmailProperty.getValue())));
            String password = new String(securityService.decrypt(secretKey, Base64.getDecoder().decode(encryptedPasswordProperty.getValue())));

            int size = articleDetailUrls.size();
            int totalItemPerThread = size / (numThread - 1);

            for (int chunk = 0; chunk < numThread; chunk++) {
                crawlDetailExecutorService.submit(new MediumDetailPageCrawlerWorker(size, chunk, totalItemPerThread, email, password, articleDetailUrls, articleDetailUrlService, articleCrawlingResultService, crawlingEventService));
            }
        } catch (Exception e) {
            log.error("Failed to crawl detail page", e);
        }
    }



    private void selectListingUrl(Set<String> urls, List<WebElement> elements) {
        for (WebElement element : elements) {
            urls.add(element.getAttribute("href"));
        }
    }

    private void saveListingUrls(Set<String> listingUrls, OriginSite originSite, BatchJobTracking batchJobTracking) {
        List<ArticleListingUrl> result = new ArrayList<>();
        for (String url : listingUrls) {
            ArticleListingUrl articleListingUrl = new ArticleListingUrl();
            articleListingUrl.setUrl(url);
            articleListingUrl.setOriginSite(originSite);
            articleListingUrl.setCreatedByJob(batchJobTracking);
            articleListingUrl.setStatus(ArticleCrawlingUrlStatusEnum.ACTIVE);
            result.add(articleListingUrl);
        }

        articleListingUrlService.batchCreateIfNotExist(result);
    }

    private void saveDetailUrls(Map<String, String> detailUrlPostIdMap, OriginSite originSite, BatchJobTracking batchJobTracking) {
        List<ArticleDetailUrl> result = new ArrayList<>();
        for (Map.Entry<String, String> entry : detailUrlPostIdMap.entrySet()) {
            ArticleDetailUrl articleDetailUrl = new ArticleDetailUrl();
            articleDetailUrl.setUrl(entry.getKey());
            articleDetailUrl.setOriginPostId(entry.getValue());
            articleDetailUrl.setOriginSite(originSite);
            articleDetailUrl.setStatus(ArticleCrawlingUrlStatusEnum.ACTIVE);
            articleDetailUrl.setCreatedByJob(batchJobTracking);
            result.add(articleDetailUrl);
        }

        articleDetailUrlService.batchCreateIfNotExist(result);
    }

    private void saveCrawlingEvent(Stack<CrawlingEvent> crawlingEvents) {
        if (!crawlingEvents.isEmpty()) {
            CrawlingEvent.complete(crawlingEvents.peek(), TrackingEventResultEnum.SUCCESS);
        }

        crawlingEventService.saveAsync(new ArrayList<>(crawlingEvents));
    }

}
