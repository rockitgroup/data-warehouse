package com.rockitgroup.infrastructure.data.warehouse.crawler.util;

import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.util.WebUtils;
import com.rockitgroup.infrastructure.data.warehouse.crawler.Constants;
import com.rockitgroup.infrastructure.data.warehouse.crawler.exception.CaptchaException;
import com.rockitgroup.infrastructure.vitamin.common.service.EmailService;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.joda.time.DateTime;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.http.HttpStatus;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
public class WebDriverUtils {

    public static WebDriver getChromeDriver() {
        return getChromeDriver(false);
    }

    public static WebDriver getChromeDriver(boolean isIncognito) {
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        if (isIncognito) {
            options.addArguments("--incognito");
        }

        return new ChromeDriver(options);
    }

    public static HttpResponse getPage(WebDriver driver, String url) {
        HttpResponse response = WebUtils.get(url);
        if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            driver.get(url);
        }
        return response;
    }

    public static HttpResponse getPage(WebDriver driver, String url, Stack<CrawlingEvent> crawlingEvents, CrawlTypeEnum crawlingEventType) {
        if (!crawlingEvents.isEmpty()) {
            CrawlingEvent latestCrawlingEvent = crawlingEvents.pop();
            latestCrawlingEvent.setCrawlEndedAt(DateTime.now());
            latestCrawlingEvent.setResult(TrackingEventResultEnum.SUCCESS);
            latestCrawlingEvent.setStatus(TrackingEventStatusEnum.DONE);

            crawlingEvents.push(latestCrawlingEvent);
        }

        CrawlingEvent crawlingEvent = CrawlingEvent.create(url, crawlingEventType);
        crawlingEvents.push(crawlingEvent);

        return getPage(driver, url);
    }

    public static WebElement waitForElement(WebDriver driver, final By locator, long timeout) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            log.error("Failed to wait for element", e);
        }
        return null;
    }

    public static void sendKeys(WebDriver driver, final By locator, String keys) {
        WebElement element = findElement(driver, locator);
        if (element != null) {
            element.sendKeys(keys);
        }
    }

    public static void click(WebDriver driver, final By locator) {
        WebElement element = findElement(driver, locator);
        if (element != null) {
            element.click();
        }
    }

    public static WebElement fluentWaitForElement(WebDriver driver, final By locator, long timeout, long pollingDuration) {
        try {
            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(Duration.ofSeconds(timeout))
                    .pollingEvery(Duration.ofSeconds(pollingDuration))
                    .ignoring(NoSuchElementException.class);

            return wait.until(driver1 -> driver1.findElement(locator));
        } catch (Exception e) {
            log.error("Failed to wait for element", e);
        }

        return null;
    }

    public static void wait(WebDriver driver, long waitTime) {
        try {
            driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("Failed to wait", e);
        }
    }

    public static WebElement findElement(WebDriver driver, By locator) {
        try {
            return driver.findElement(locator);
        } catch (Exception e) {
            log.warn("Cannot find element: " + locator.toString());
        }
        return null;
    }

    public static List<WebElement> findElements(WebDriver driver, By locator) {
        try {
            return driver.findElements(locator);
        } catch (Exception e) {
            log.warn("Cannot find element: " + locator.toString());
        }
        return new ArrayList<>();
    }

    public static  String findElementTextByClass(Element element, String className, String position) {
        Elements foundElements = element.getElementsByClass(className);
        Element foundElement = null;
        if (Objects.equals(position, Constants.LAST_POSITION)) {
            foundElement = foundElements.last();
        } else if (Objects.equals(position, Constants.FIRST_POSITION)) {
            foundElement = foundElements.first();
        }

        if (foundElement != null) {
            return foundElement.text().trim();
        }

        return null;
    }

    public static  String findElementTextByClassAndTag(Element element, String className, String classPosition, String tagName, String tagPosition) {
        Elements foundElements = element.getElementsByClass(className);
        Element foundElement = null;
        if (Objects.equals(classPosition, Constants.LAST_POSITION)) {
            foundElement = foundElements.last();
        } else if (Objects.equals(classPosition, Constants.FIRST_POSITION)) {
            foundElement = foundElements.first();
        }

        if (foundElement != null) {
            Elements foundTagElements = foundElement.getElementsByTag(tagName);
            Element foundTagElement = null;
            if (Objects.equals(tagPosition, Constants.LAST_POSITION)) {
                foundTagElement = foundTagElements.last();
            } else if (Objects.equals(tagPosition, Constants.FIRST_POSITION)) {
                foundTagElement = foundTagElements.first();
            }

            if (foundTagElement != null) {
                return foundTagElement.text().trim();
            }
        }

        return null;
    }

    public static  String findElementTextByTwoLevelClasses(Element element, String firstClassName, String firstClassPosition, String secondClassName, String secondClassPosition) {
        Elements foundElements = element.getElementsByClass(firstClassName);
        Element foundElement = null;
        if (Objects.equals(firstClassPosition, Constants.LAST_POSITION)) {
            foundElement = foundElements.last();
        } else if (Objects.equals(firstClassPosition, Constants.FIRST_POSITION)) {
            foundElement = foundElements.first();
        }

        if (foundElement != null) {
            Elements foundFinalElements = foundElement.getElementsByClass(secondClassName);
            Element foundFinalElement = null;
            if (Objects.equals(secondClassPosition, Constants.LAST_POSITION)) {
                foundFinalElement = foundFinalElements.last();
            } else if (Objects.equals(secondClassPosition, Constants.FIRST_POSITION)) {
                foundFinalElement = foundFinalElements.first();
            }

            if (foundFinalElement != null) {
                return foundFinalElement.text().trim();
            }
        }

        return null;
    }

    public static  String findElementTextByClassAndAttr(Element element, String className, String classPosition, String attrName) {
        Elements foundElements = element.getElementsByClass(className);
        Element foundElement = null;
        if (Objects.equals(classPosition, Constants.LAST_POSITION)) {
            foundElement = foundElements.last();
        } else if (Objects.equals(classPosition, Constants.FIRST_POSITION)) {
            foundElement = foundElements.first();
        }

        if (foundElement != null && foundElement.attr(attrName) != null) {
            return foundElement.attr(attrName).trim();
        }

        return null;
    }

    public static  String findElementAttributeByClassAndTag(Element element, String className, String classPosition, String tagName, String tagPosition, String attributeName) {
        Elements foundElements = element.getElementsByClass(className);
        Element foundElement = null;
        if (Objects.equals(classPosition, Constants.LAST_POSITION)) {
            foundElement = foundElements.last();
        } else if (Objects.equals(classPosition, Constants.FIRST_POSITION)) {
            foundElement = foundElements.first();
        }

        if (foundElement != null) {
            Elements foundTagElements = foundElement.getElementsByTag(tagName);
            Element foundTagElement = null;
            if (Objects.equals(tagPosition, Constants.LAST_POSITION)) {
                foundTagElement = foundTagElements.last();
            } else if (Objects.equals(tagPosition, Constants.FIRST_POSITION)) {
                foundTagElement = foundTagElements.first();
            }

            if (foundTagElement != null) {
                return foundTagElement.attr(attributeName);
            }
        }

        return null;
    }

    public static void checkCaptcha(EmailService emailService, InternetAddress fromEmail, List<String> adminEmails, WebDriver driver, By captchaFormLocator, int maxCaptchaWaitTries, long captchaHandleTimeWait) throws CaptchaException, InterruptedException, MessagingException, IOException, TemplateException {
        int captchaWaitTry = 0;
        while (findElement(driver, captchaFormLocator) != null) {
            if (captchaWaitTry > maxCaptchaWaitTries) {
                emailService.sendEmails(fromEmail, adminEmails, Constants.CAPTCHA_FORM_NOTICE_EMAIL_SUBJECT, Constants.CAPTCHA_FORM_NOTICE_EMAIL_TEMPLATE, new HashMap<>(), false);
                throw new CaptchaException();
            }

            emailService.sendEmails(fromEmail, adminEmails, Constants.CAPTCHA_FORM_NOTICE_EMAIL_SUBJECT, Constants.CAPTCHA_FORM_NOTICE_EMAIL_TEMPLATE, new HashMap<>(), false);
            Thread.sleep(captchaHandleTimeWait * 1000);
            captchaWaitTry++;
        }
    }
}
