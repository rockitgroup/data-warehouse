package com.rockitgroup.infrastructure.data.warehouse.crawler;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/27/18
 * Time: 1:18 AM
 */
public class Constants {

    public static final String SCHEDULER_PROPERTIES_FILE_PATH = "quartz.properties";

    public static final String CHROME_DRIVER_FILE_PATH = "driver/chromedriver";

    public static final String CAPTCHA_FORM_NOTICE_EMAIL_SUBJECT= "Captcha form required";
    public static final String CAPTCHA_FORM_NOTICE_EMAIL_TEMPLATE = "captcha-form-notice-template.ftl";

    public static final long DEFAULT_TIME_WAIT_FOR_ELEMENT = 10;
    public static final long DEFAULT_CAPTCHA_HANDLE_TIME_WAIT = 75;
    public static final int MAX_CAPTCHA_WAIT_TRIES = 10;

    public static final String LAST_POSITION = "last";
    public static final String FIRST_POSITION = "first";

}
