package com.rockitgroup.infrastructure.data.warehouse.crawler.scheduler.app;

import com.rockitgroup.infrastructure.data.warehouse.common.service.BatchJobTrackingService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.CrawlingEventService;
import com.rockitgroup.infrastructure.vitamin.common.service.EmailService;
import com.rockitgroup.infrastructure.vitamin.common.service.HttpClientService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import java.util.List;

@Slf4j
public class TranslationJob implements Job {


    @Autowired
    private EmailService emailService;

    @Autowired
    @Qualifier("crawlMedium.homePageUrl")
    private String homePageUrl;

    @Autowired
    @Qualifier("crawlMedium.topicPageUrl")
    private String topicPageUrl;

    @Autowired
    @Qualifier("crawlMedium.email")
    private String email;

    @Autowired
    @Qualifier("crawlMedium.password")
    private String password;

    @Autowired
    @Qualifier("fromEmail")
    private InternetAddress fromEmail;

    @Autowired
    private HttpClientService httpClientService;

    @Autowired
    private BatchJobTrackingService batchJobTrackingService;

    @Autowired
    private CrawlingEventService crawlingEventService;


    // Resource is for map and list
    @Resource(name = "crawlMedium.topics")
    private List<String> topics;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
//        BatchJobTracking batchJobTracking = BatchJobTracking.create(context.getJobDetail().getKey().getName());
//        Stack<CrawlingEvent> crawlingEvents = new Stack<>();
//        try {
//
//            batchJobTrackingService.saveAsync(batchJobTracking);
//
//            WebDriver driver = WebDriverUtils.getChromeDriver();
//
//            WebDriverUtils.getPage(driver, "https://translate.google.com/#view=home&op=translate&sl=en&tl=vi&text=", crawlingEvents, CrawlTypeEnum.APP);
//            WebDriverUtils.waitForElement(driver, By.cssSelector("textarea#source"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
//
//            driver.findElement(By.cssSelector("textarea#source")).sendKeys("Reflecting on My Failure to Build a Billion-Dollar Company");
//
//            WebDriverUtils.waitForElement(driver, By.cssSelector(".tlid-result span.translation"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
//
//            String translation = driver.findElement(By.cssSelector(".tlid-result span.translation")).getText();
//
//            System.out.println(translation);
//
//            Thread.sleep(20000);
//
//            driver.close();
//
//            CrawlingEvent.complete(crawlingEvents.peek(), TrackingEventResultEnum.SUCCESS);
//
//            batchJobTracking.setResult(TrackingEventResultEnum.SUCCESS);
//
//        } catch (CaptchaException e) {
//            log.error("Captcha appear");
//            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
//            batchJobTracking.setOutput(e.getMessage());
//        } catch (Exception e) {
//            log.error("Failed to run crawlApp Job", e);
//            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
//            batchJobTracking.setOutput(e.getMessage());
//        }
//
//        batchJobTracking.setStatus(TrackingEventStatusEnum.DONE);
//        batchJobTrackingService.saveAsync(batchJobTracking);
//
//        crawlingEventService.saveAsync(new ArrayList<>(crawlingEvents));
    }
}
