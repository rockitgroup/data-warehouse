package com.rockitgroup.infrastructure.data.warehouse.crawler.service.worker;


import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleCrawlingResult;
import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleDetailUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingResultStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.service.ArticleCrawlingResultService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.ArticleDetailUrlService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.CrawlingEventService;
import com.rockitgroup.infrastructure.data.warehouse.crawler.Constants;
import com.rockitgroup.infrastructure.data.warehouse.crawler.util.WebDriverUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Slf4j
public class MediumDetailPageCrawlerWorker implements Runnable{

    private Integer size;
    private Integer chunk;
    private Integer totalItemPerThread;
    private String email;
    private String password;
    private List<ArticleDetailUrl> articleDetailUrls;
    private ArticleDetailUrlService articleDetailUrlService;
    private ArticleCrawlingResultService articleCrawlingResultService;
    private CrawlingEventService crawlingEventService;


    public MediumDetailPageCrawlerWorker(Integer size, Integer chunk, Integer totalItemPerThread, String email, String password, List<ArticleDetailUrl> articleDetailUrls, ArticleDetailUrlService articleDetailUrlService, ArticleCrawlingResultService articleCrawlingResultService, CrawlingEventService crawlingEventService) {
        this.size = size;
        this.chunk = chunk;
        this.totalItemPerThread = totalItemPerThread;
        this.email = email;
        this.password = password;
        this.articleDetailUrls = articleDetailUrls;
        this.articleDetailUrlService = articleDetailUrlService;
        this.articleCrawlingResultService = articleCrawlingResultService;
        this.crawlingEventService = crawlingEventService;
    }

    @Override
    public void run() {
        Stack<CrawlingEvent> crawlingEvents = new Stack<>();
        WebDriver driver = WebDriverUtils.getChromeDriver();
        for (int j = chunk * totalItemPerThread; j < (chunk + 1) * totalItemPerThread; j++) {
            if (size > j) {
                ArticleDetailUrl articleDetailUrl = articleDetailUrls.get(j);

                try {
                    HttpResponse response = WebDriverUtils.getPage(driver, articleDetailUrl.getUrl(), crawlingEvents, CrawlTypeEnum.ARTICLE_DETAIL_PAGE);

                    if (response.getStatusLine().getStatusCode() == HttpStatus.GONE.value())  {
                        articleDetailUrl.setStatus(ArticleCrawlingUrlStatusEnum.INACTIVE);
                        articleDetailUrlService.save(articleDetailUrl);
                        continue;
                    } else if (response.getStatusLine().getStatusCode() == HttpStatus.TOO_MANY_REQUESTS.value()) {
                        Thread.sleep(Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT * 1000);
                        WebDriverUtils.getPage(driver, articleDetailUrl.getUrl(), crawlingEvents, CrawlTypeEnum.ARTICLE_DETAIL_PAGE);
                    }

                    if (isSuspendedPage(driver)) {
                        articleDetailUrl.setStatus(ArticleCrawlingUrlStatusEnum.INACTIVE);
                        articleDetailUrlService.save(articleDetailUrl);
                        continue;
                    }

                    login(driver, email, password);
                    WebDriverUtils.waitForElement(driver, By.cssSelector("time"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
                    WebElement publishedAtElement = WebDriverUtils.findElement(driver, By.cssSelector("time"));

                    WebDriverUtils.waitForElement(driver, By.cssSelector("span.readingTime[title]"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
                    WebElement readingTimeElement = WebDriverUtils.findElement(driver, By.cssSelector("span.readingTime[title]"));

                    WebDriverUtils.waitForElement(driver, By.cssSelector("section.section--body"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
                    List<WebElement> mainContent = WebDriverUtils.findElements(driver, By.cssSelector("section.section--body"));

                    ArticleCrawlingResult articleCrawlingResult = new ArticleCrawlingResult();
                    articleCrawlingResult.setUrl(articleDetailUrl.getUrl());
                    articleCrawlingResult.setTitle(getArticleTitle(mainContent.get(0)));
                    articleCrawlingResult.setPublishedTime(getPublishedTime(publishedAtElement));
                    articleCrawlingResult.setReadingTime(getReadingTime(readingTimeElement));
                    articleCrawlingResult.setOriginHtmlContent(getArticleOriginalContent(mainContent));
                    articleCrawlingResult.setMainImageUrl(getArticleMainImageUrl(mainContent.get(0)));
                    articleCrawlingResult.setStatus(ArticleCrawlingResultStatusEnum.ACTIVE);
                    articleCrawlingResult = (ArticleCrawlingResult) articleCrawlingResultService.save(articleCrawlingResult);

                    articleDetailUrl.setCrawlingResult(articleCrawlingResult);
                    articleDetailUrl.setStatus(ArticleCrawlingUrlStatusEnum.CRAWLED);

                    articleDetailUrlService.save(articleDetailUrl);
                } catch (Exception e) {
                    log.error("Failed to crawl page " + articleDetailUrl.getUrl(), e);
                }
            }
        }

        driver.close();

        if (!crawlingEvents.isEmpty()) {
            CrawlingEvent.complete(crawlingEvents.peek(), TrackingEventResultEnum.SUCCESS);
        }

        crawlingEventService.saveAsync(new ArrayList<>(crawlingEvents));
    }

    private boolean isSuspendedPage(WebDriver driver) {
        try {
            return driver.getTitle().toLowerCase().contains("suspended");
        } catch (Exception e) {
            log.error("Failed to check title", e);
        }
        return false;
    }

    private void login(WebDriver driver, String email, String password) {
        WebElement signInButton = WebDriverUtils.findElement(driver, By.className("js-signInButton"));
        if (signInButton != null) {
            signInButton.click();

            WebDriverUtils.waitForElement(driver, By.cssSelector(".js-facebookButton[data-action-value='login']"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
            WebDriverUtils.click(driver, By.cssSelector(".js-facebookButton[data-action-value='login']"));
            WebDriverUtils.waitForElement(driver, By.id("loginbutton"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

            WebDriverUtils.sendKeys(driver, By.id("email"), email);
            WebDriverUtils.sendKeys(driver, By.id("pass"), password);
            WebDriverUtils.click(driver, By.id("loginbutton"));
        }
    }

    private String getArticleMainImageUrl(WebElement mainContent) {
        try {
            if (mainContent == null) {
                throw new Exception("Cannot find mainContent");
            }
            WebElement mainImageUrlElement = mainContent.findElement(By.cssSelector("figure img.progressiveMedia-image"));
            if (mainImageUrlElement != null) {
                return mainImageUrlElement.getAttribute("src");
            } else {
                mainImageUrlElement = mainContent.findElement(By.cssSelector("div.elevateCover-image"));
                if (mainImageUrlElement != null) {
                    String style = mainImageUrlElement.getAttribute("style");
                    if (StringUtils.isNotEmpty(style)) {
                        int firstIndex = style.indexOf("https://");
                        int lastIndex = style.indexOf("\");", firstIndex);
                        return style.substring(firstIndex, lastIndex);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Failed to get article main image url", e);
        }
        return null;
    }

    private String getArticleOriginalContent(List<WebElement> mainContentElements) {
        StringBuilder htmlContentBuilder = new StringBuilder();
        try {
            if (mainContentElements == null || mainContentElements.isEmpty()) {
                throw new Exception("Cannot find mainContent");
            }

            for (WebElement mainContent : mainContentElements) {
                WebElement sectionContent = mainContent.findElement(By.cssSelector("div.section-content"));
                if (sectionContent != null) {
                    htmlContentBuilder.append(sectionContent.getAttribute("innerHTML"));
                }
            }

        } catch (Exception e) {
            log.error("Failed to get article original content", e);
        }
        return htmlContentBuilder.toString();
    }

    private String getReadingTime(WebElement readingTimeElement) {
        try {
            if (readingTimeElement == null) {
                throw new Exception("Cannot find readingTime");
            }
            String readingTime = readingTimeElement.getAttribute("title");
            if (StringUtils.isEmpty(readingTime)) {
                readingTime = readingTimeElement.getText();
            }
            return readingTime;
        } catch (Exception e) {
            log.error("Failed to get article reading time", e);
        }
        return null;
    }

    private String getArticleTitle(WebElement mainContent) {
        try {
            if (mainContent == null) {
                throw new Exception("Cannot find mainContent");
            }
            WebElement titleElement = mainContent.findElement(By.cssSelector("h1.graf--title"));
            if (titleElement != null) {
                return titleElement.getText();
            }
        } catch (Exception e) {
            log.error("Failed to get article title", e);
        }
        return StringUtils.EMPTY;
    }

    private String getPublishedTime(WebElement publishedTimeElement) {
        try {
            if (publishedTimeElement == null) {
                throw new Exception("Cannot find publishedTime");
            }
            String publishedTime = publishedTimeElement.getAttribute("datetime");
            if (StringUtils.isEmpty(publishedTime)) {
                publishedTime = publishedTimeElement.getText();
            }
            return publishedTime;
        } catch (Exception e) {
            log.error("Failed to get article published time", e);
        }
        return null;
    }
}
