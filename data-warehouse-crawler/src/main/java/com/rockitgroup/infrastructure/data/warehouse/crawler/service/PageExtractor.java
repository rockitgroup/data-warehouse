package com.rockitgroup.infrastructure.data.warehouse.crawler.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import org.openqa.selenium.WebDriver;

public interface PageExtractor {

    void findListingPageUrlsFromHomePage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking);

    void findDetailPageUrlsFromHomePage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking);

    void findDetailPageUrlsFromListingPage(WebDriver driver, OriginSite originSite, BatchJobTracking batchJobTracking);

    void crawlDetailPage(OriginSite originSite, BatchJobTracking batchJobTracking);
}
