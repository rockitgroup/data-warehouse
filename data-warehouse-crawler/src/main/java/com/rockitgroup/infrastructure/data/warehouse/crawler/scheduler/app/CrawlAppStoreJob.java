package com.rockitgroup.infrastructure.data.warehouse.crawler.scheduler.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppPlatformDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRankingChangeRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRankingRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.AppRankingTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.service.BatchJobTrackingService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.CrawlingEventService;
import com.rockitgroup.infrastructure.data.warehouse.common.util.DateUtils;
import com.rockitgroup.infrastructure.data.warehouse.common.util.MapperUtils;
import com.rockitgroup.infrastructure.data.warehouse.crawler.Constants;
import com.rockitgroup.infrastructure.data.warehouse.crawler.exception.CaptchaException;
import com.rockitgroup.infrastructure.data.warehouse.crawler.util.NumberUtils;
import com.rockitgroup.infrastructure.data.warehouse.crawler.util.WebDriverUtils;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.service.EmailService;
import com.rockitgroup.infrastructure.vitamin.common.service.HttpClientService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import java.text.NumberFormat;
import java.util.*;

@Slf4j
public class CrawlAppStoreJob implements Job {

    private static final String RANK_VALUE_SORT_TYPE = "rank";
    private static final String RANK_CHANGE_SORT_TYPE = "variation";


    @Autowired
    private EmailService emailService;

    @Autowired
    @Qualifier("crawlApp.homePageUrl")
    private String homePageUrl;

    @Autowired
    @Qualifier("crawlApp.loginPageUrl")
    private String loginPageUrl;

    @Autowired
    @Qualifier("crawlApp.email")
    private String email;

    @Autowired
    @Qualifier("crawlApp.password")
    private String password;

    @Autowired
    @Qualifier("crawlApp.listPlatformApiUrl")
    private String listPlatformApiUrl;

    @Autowired
    @Qualifier("crawlApp.saveAppApiUrl")
    private String saveAppApiUrl;

    @Autowired
    @Qualifier("crawlApp.saveAppRankingApiUrl")
    private String saveAppRankingApiUrl;

    @Autowired
    @Qualifier("crawlApp.saveAppRankingChangeApiUrl")
    private String saveAppRankingChangeApiUrl;

    @Autowired
    @Qualifier("fromEmail")
    private InternetAddress fromEmail;

    @Autowired
    private HttpClientService httpClientService;

    @Autowired
    private BatchJobTrackingService batchJobTrackingService;

    @Autowired
    private CrawlingEventService crawlingEventService;


    // Resource is for map and list
    @Resource(name = "crawlApp.topChartPageTemplateUrlMap")
    private Map<String, String> topChartPageTemplateUrlMap;

    @Resource(name = "crawlApp.countryCodes")
    private List<String> countryCodes;

    @Resource(name = "adminEmails")
    private List<String> adminEmails;

    @Resource(name = "crawlApp.lookUpAppDetailUrlMap")
    private Map<String, String> lookUpAppDetailUrlMap;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        BatchJobTracking batchJobTracking = BatchJobTracking.create(context.getJobDetail().getKey().getName());
        Stack<CrawlingEvent> crawlingEvents = new Stack<>();
        try {

            batchJobTrackingService.saveAsync(batchJobTracking);

            RestTemplate restTemplate = new RestTemplate();
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

            ResponseDTO response = restTemplate.getForObject(listPlatformApiUrl, ResponseDTO.class);
            if (response == null) {
                log.error("Failed to retrieve list platform. Error message: Response is null");
                return;
            } else if (!response.isSuccess() || response.getObject() == null) {
                log.error("Failed to retrieve list platform. Error message: " + response.getError());
                return;
            }

            String appPlatformResult = MapperUtils.getObjectMapper().writeValueAsString(response.getObject());
            List<AppPlatformDTO> appPlatformList = MapperUtils.getObjectMapper().readValue(appPlatformResult, new TypeReference<List<AppPlatformDTO>>(){});

            WebDriver driver = WebDriverUtils.getChromeDriver();

            WebDriverUtils.getPage(driver, loginPageUrl, crawlingEvents, CrawlTypeEnum.APP);

            WebDriverUtils.checkCaptcha(emailService, fromEmail, adminEmails, driver, By.id("captcha_form"), Constants.MAX_CAPTCHA_WAIT_TRIES, Constants.DEFAULT_CAPTCHA_HANDLE_TIME_WAIT);

            WebDriverUtils.waitForElement(driver, By.id("submit"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

            driver.findElement(By.id("email")).sendKeys(email);
            driver.findElement(By.id("password")).sendKeys(password);
            driver.findElement(By.id("submit")).click();

            WebDriverUtils.checkCaptcha(emailService, fromEmail, adminEmails, driver, By.id("captcha_form"), Constants.MAX_CAPTCHA_WAIT_TRIES, Constants.DEFAULT_CAPTCHA_HANDLE_TIME_WAIT);

            WebDriverUtils.waitForElement(driver, By.className("user-email"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);

            for (AppPlatformDTO appPlatform : appPlatformList) {
                if (topChartPageTemplateUrlMap.containsKey(appPlatform.getKey())) {
                    String url = topChartPageTemplateUrlMap.get(appPlatform.getKey());

                    for (AppRankingTypeEnum rankType : AppRankingTypeEnum.ALL_RANKING_TYPES) {

                        Map<SaveAppRequestDTO.AppRequestDTO, String> appRequestLinkMap = new HashMap<>();
                        Map<SaveAppRequestDTO.AppRequestDTO, Integer> appRequestRankMap = new HashMap<>();
                        Map<SaveAppRequestDTO.AppRequestDTO, RankingChange> appRequestRankingChangeMap = new HashMap<>();

                        for (String countryCode : countryCodes) {
                            String topChartValueRankingUrl = getTopChartUrl(url, countryCode, rankType.name(), RANK_VALUE_SORT_TYPE);

                            WebDriverUtils.getPage(driver, topChartValueRankingUrl, crawlingEvents, CrawlTypeEnum.APP);

                            WebDriverUtils.checkCaptcha(emailService, fromEmail, adminEmails, driver, By.id("captcha_form"), Constants.MAX_CAPTCHA_WAIT_TRIES, Constants.DEFAULT_CAPTCHA_HANDLE_TIME_WAIT);
                            setAppRankingRequestDTODataMap(driver, appPlatform.getKey(), appRequestLinkMap, appRequestRankMap);

                            topChartValueRankingUrl = getTopChartUrl(url, countryCode, rankType.name(), RANK_CHANGE_SORT_TYPE);

                            WebDriverUtils.getPage(driver, topChartValueRankingUrl, crawlingEvents, CrawlTypeEnum.APP);

                            WebDriverUtils.checkCaptcha(emailService, fromEmail, adminEmails, driver, By.id("captcha_form"), Constants.MAX_CAPTCHA_WAIT_TRIES, Constants.DEFAULT_CAPTCHA_HANDLE_TIME_WAIT);
                            setAppRankingChangeRequestDTODataMap(driver, appPlatform.getKey(), appRequestLinkMap, appRequestRankingChangeMap);

                            SaveAppRequestDTO saveAppRequestDTO = new SaveAppRequestDTO();
                            List<SaveAppRequestDTO.AppRequestDTO> appRequestDTOList = new ArrayList<>();
                            for (Map.Entry<SaveAppRequestDTO.AppRequestDTO, String> entry : appRequestLinkMap.entrySet()) {
                                try {
                                    SaveAppRequestDTO.AppRequestDTO appRequestDTO = entry.getKey();

                                    if (StringUtils.isNotEmpty(appRequestDTO.getStoreAppId())) {
                                        try {
                                            String lookUpUrl = String.format(lookUpAppDetailUrlMap.get(appRequestDTO.getPlatformKey().toUpperCase()), appRequestDTO.getStoreAppId());
                                            JSONObject jsonResult = httpClientService.getJson(lookUpUrl);

                                            JSONObject appDataJson = jsonResult.getJSONArray("results").getJSONObject(0);
                                            appRequestDTO.setUrl(appDataJson.getString("trackViewUrl"));
                                            appRequestDTO.setDescription(appDataJson.getString("description"));
                                            appRequestDTO.setPrice(appDataJson.getDouble("price"));
                                            appRequestDTO.setSize(FileUtils.byteCountToDisplaySize(appDataJson.getLong("fileSizeBytes")));
                                        } catch (Exception e) {
                                            log.error("Failed to look up app id", e);
                                        }
                                    }

                                    if (appRequestDTO.getPrice() == null) {
                                        WebDriverUtils.getPage(driver, homePageUrl + entry.getValue(), crawlingEvents, CrawlTypeEnum.APP);

                                        WebDriverUtils.checkCaptcha(emailService, fromEmail, adminEmails, driver, By.id("captcha_form"), Constants.MAX_CAPTCHA_WAIT_TRIES, Constants.DEFAULT_CAPTCHA_HANDLE_TIME_WAIT);

                                        WebElement appDetailElement = WebDriverUtils.waitForElement(driver, By.className("app-details"), 10);
                                        if (appDetailElement != null) {

                                            int numTryLoadLinkSection = 0;
                                            WebElement appDetailUrlElement = WebDriverUtils.fluentWaitForElement(driver, By.className("link-section"), 30, 2);
                                            while (appDetailUrlElement == null) {
                                                if (numTryLoadLinkSection > 10) {
                                                    throw new Exception(String.format("Cannot find url for app %s", appRequestDTO.getName()));
                                                }
                                                Thread.sleep(5 * 1000);
                                                appDetailUrlElement = WebDriverUtils.fluentWaitForElement(driver, By.className("link-section"), 30, 2);
                                                numTryLoadLinkSection++;
                                            }

                                            Document appDetailUrl = Jsoup.parse(appDetailUrlElement.getAttribute("innerHTML"));
                                            appRequestDTO.setUrl(appDetailUrl.getElementsByTag("a").last().attr("app-confirm"));

                                            Document appDetail = Jsoup.parse(appDetailElement.getAttribute("innerHTML"));
                                            appRequestDTO.setDescription(WebDriverUtils.findElementTextByTwoLevelClasses(appDetail.getElementById("app_content"), "app_content_section", Constants.FIRST_POSITION, "desc", Constants.FIRST_POSITION));

                                            String priceText = WebDriverUtils.findElementTextByTwoLevelClasses(appDetail, "data-section", Constants.FIRST_POSITION, "item", Constants.FIRST_POSITION);
                                            Double price = NumberUtils.findDoubleFromString(priceText);

                                            appRequestDTO.setPrice(price);

                                            String sizeText = appDetail.getElementsContainingText("Size:").last().parent().text();

                                            if (sizeText != null) {
                                                appRequestDTO.setSize(sizeText.replace("Size:", ""));
                                            }
                                        }


                                    }

                                    appRequestDTOList.add(appRequestDTO);

                                } catch (Exception e) {
                                    log.error("Failed to get app request data", e);
                                }
                            }

                            saveAppRequestDTO.setApps(appRequestDTOList);

                            HttpEntity<SaveAppRequestDTO> request = new HttpEntity<>(saveAppRequestDTO, headers);
                            ResponseDTO responseDTO = restTemplate.postForObject(saveAppApiUrl, request, ResponseDTO.class);

                            if (responseDTO.isSuccess()) {
                                String saveAppResult = MapperUtils.getObjectMapper().writeValueAsString(responseDTO.getObject());
                                Map<String, Object> saveAppData = MapperUtils.getObjectMapper().readValue(saveAppResult, new TypeReference<Map<String, Object>>() {});

                                String successSavedAppStr = MapperUtils.getObjectMapper().writeValueAsString(saveAppData.get("saved"));
                                List<AppDTO> successSavedApp = MapperUtils.getObjectMapper().readValue(successSavedAppStr, new TypeReference<List<AppDTO>>() {});

                                Map<String, Long> successSavedAppIdMap = new HashMap<>();
                                for (AppDTO appDTO : successSavedApp) {
                                    successSavedAppIdMap.put(appDTO.getName() + com.rockitgroup.infrastructure.data.warehouse.common.Constants.CACHE_KEY_SEPARATOR + appDTO.getPlatform().getKey(), appDTO.getId());
                                }

                                String category = "Games";

                                saveAppRanking(restTemplate, headers, category, rankType.name(), countryCode, appRequestRankMap, successSavedAppIdMap);
                                saveAppRankingChange(restTemplate, headers, category, rankType.name(), countryCode, appRequestRankingChangeMap, successSavedAppIdMap);

                            } else {
                                log.error("Failed to save app:\n" + responseDTO.getError().getMessage());
                            }
                        }
                    }
                }
            }

            driver.close();

            CrawlingEvent.complete(crawlingEvents.peek(), TrackingEventResultEnum.SUCCESS);

            batchJobTracking.setResult(TrackingEventResultEnum.SUCCESS);

        } catch (CaptchaException e) {
            log.error("Captcha appear");
            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
            batchJobTracking.setOutput(e.getMessage());
        } catch (Exception e) {
            log.error("Failed to run crawlApp Job", e);
            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
            batchJobTracking.setOutput(e.getMessage());
        }

        batchJobTracking.setStatus(TrackingEventStatusEnum.DONE);
        batchJobTrackingService.saveAsync(batchJobTracking);

        crawlingEventService.saveAsync(new ArrayList<>(crawlingEvents));
    }

    private String getTopChartUrl(String templateUrl, String countryCode, String rankType, String sortType) {
        Map<String, String> values = new HashMap<>();
        values.put("countryCode", countryCode);
        values.put("rankType", StringUtils.capitalize(rankType.toLowerCase()));
        values.put("sortType", sortType);
        values.put("date", DateUtils.formatDate(DateTime.now()));
        StrSubstitutor sub = new StrSubstitutor(values, "%(", ")");
        return sub.replace(templateUrl);
    }

    private Double parseDouble(String input) {
        if (input == null) {
            return -1.0;
        }

        try {
            return NumberFormat.getNumberInstance(java.util.Locale.US).parse(input).doubleValue();
        } catch (Exception e) {
            return -1.0;
        }
    }

    private Integer parseInt(String input) {
        if (input == null) {
            return -1;
        }

        try {
            return NumberFormat.getNumberInstance(java.util.Locale.US).parse(input).intValue();
        } catch (Exception e) {
            return -1;
        }
    }

    private void saveAppRanking(RestTemplate restTemplate, MultiValueMap<String, String> headers, String category, String rankingType, String country, Map<SaveAppRequestDTO.AppRequestDTO, Integer> appRequestRankMap, Map<String, Long> successSavedAppIdMap) {
        SaveAppRankingRequestDTO saveAppRankingRequestDTO = new SaveAppRankingRequestDTO();
        List<SaveAppRankingRequestDTO.AppRankingRequestDTO> appRankingRequestDTOs = new ArrayList<>();
        for (Map.Entry<SaveAppRequestDTO.AppRequestDTO, Integer> entry : appRequestRankMap.entrySet()) {
            String key = entry.getKey().getName() + com.rockitgroup.infrastructure.data.warehouse.common.Constants.CACHE_KEY_SEPARATOR + entry.getKey().getPlatformKey();
            if (successSavedAppIdMap.containsKey(key)) {

                SaveAppRankingRequestDTO.AppRankingRequestDTO appRankingRequestDTO = new SaveAppRankingRequestDTO.AppRankingRequestDTO();
                appRankingRequestDTO.setAppId(successSavedAppIdMap.get(key));
                appRankingRequestDTO.setCategory(category);
                appRankingRequestDTO.setCountry(country);
                appRankingRequestDTO.setPlatformKey(entry.getKey().getPlatformKey());
                appRankingRequestDTO.setRankDate(DateTime.now());
                appRankingRequestDTO.setRanking(entry.getValue());
                appRankingRequestDTO.setRankingType(rankingType);

                appRankingRequestDTOs.add(appRankingRequestDTO);
            }
        }

        saveAppRankingRequestDTO.setAppRankings(appRankingRequestDTOs);

        HttpEntity<SaveAppRankingRequestDTO> saveAppRankingRequest = new HttpEntity<>(saveAppRankingRequestDTO, headers);
        restTemplate.postForObject(saveAppRankingApiUrl, saveAppRankingRequest, ResponseDTO.class);
    }

    private void saveAppRankingChange(RestTemplate restTemplate, MultiValueMap<String, String> headers, String category, String rankingType, String country, Map<SaveAppRequestDTO.AppRequestDTO, RankingChange> appRankChangeMap, Map<String, Long> successSavedAppIdMap) {
        SaveAppRankingChangeRequestDTO saveAppRankingChangeRequestDTO = new SaveAppRankingChangeRequestDTO();
        List<SaveAppRankingChangeRequestDTO.AppRankingChangeRequestDTO> appRankingRequestDTOs = new ArrayList<>();
        for (Map.Entry<SaveAppRequestDTO.AppRequestDTO, RankingChange> entry : appRankChangeMap.entrySet()) {
            String key = entry.getKey().getName() + com.rockitgroup.infrastructure.data.warehouse.common.Constants.CACHE_KEY_SEPARATOR + entry.getKey().getPlatformKey();
            if (successSavedAppIdMap.containsKey(key)) {

                SaveAppRankingChangeRequestDTO.AppRankingChangeRequestDTO appRankingChangeRequestDTO = new SaveAppRankingChangeRequestDTO.AppRankingChangeRequestDTO();
                appRankingChangeRequestDTO.setAppId(successSavedAppIdMap.get(key));
                appRankingChangeRequestDTO.setCategory(category);
                appRankingChangeRequestDTO.setCountry(country);
                appRankingChangeRequestDTO.setPlatformKey(entry.getKey().getPlatformKey());
                appRankingChangeRequestDTO.setRankDate(DateTime.now());
                appRankingChangeRequestDTO.setNewRanking(entry.getValue().getNewRanking());
                appRankingChangeRequestDTO.setRankChange(entry.getValue().getRankChange());
                appRankingChangeRequestDTO.setRankingType(rankingType);

                appRankingRequestDTOs.add(appRankingChangeRequestDTO);
            }
        }

        saveAppRankingChangeRequestDTO.setAppRankingChangeRequests(appRankingRequestDTOs);

        HttpEntity<SaveAppRankingChangeRequestDTO> saveAppRankingChangeRequest = new HttpEntity<>(saveAppRankingChangeRequestDTO, headers);
        restTemplate.postForObject(saveAppRankingChangeApiUrl, saveAppRankingChangeRequest, ResponseDTO.class);
    }

    private void setAppRankingRequestDTODataMap(WebDriver driver, String platformKey, Map<SaveAppRequestDTO.AppRequestDTO, String> appRequestLinkMap, Map<SaveAppRequestDTO.AppRequestDTO, Integer> appRequestRankMap) {
        WebElement table = WebDriverUtils.waitForElement(driver, By.className("scroll-table-container"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
        if (table != null) {
            Document page = Jsoup.parse(table.getAttribute("innerHTML"));
            Elements elements = page.getElementsByClass("main-row");

            for (Element element : elements) {
                try {
                    String rank = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-sort-number", Constants.LAST_POSITION, "span", Constants.LAST_POSITION);
                    String detailLink = WebDriverUtils.findElementTextByClassAndAttr(element, "icon-link", Constants.LAST_POSITION, "href");

                    SaveAppRequestDTO.AppRequestDTO appRequestDTO = setAppRequestDTOBasicInfo(element, platformKey);

                    appRequestLinkMap.put(appRequestDTO, detailLink);
                    appRequestRankMap.put(appRequestDTO, NumberFormat.getNumberInstance(java.util.Locale.US).parse((rank)).intValue());
                } catch (Exception e) {
                    log.error("Failed to parse app request element", e);
                }
            }
        }
    }

    private void setAppRankingChangeRequestDTODataMap(WebDriver driver, String platformKey, Map<SaveAppRequestDTO.AppRequestDTO, String> appRequestLinkMap, Map<SaveAppRequestDTO.AppRequestDTO, RankingChange> appRequestRankChangeMap) {
        WebElement table = WebDriverUtils.waitForElement(driver, By.className("scroll-table-container"), Constants.DEFAULT_TIME_WAIT_FOR_ELEMENT);
        if (table != null) {
            Document page = Jsoup.parse(table.getAttribute("innerHTML"));
            Elements elements = page.getElementsByClass("main-row");

            for (Element element : elements) {
                try {
                    String newRankingText = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-rank-and-changes--number", Constants.FIRST_POSITION, "span", Constants.LAST_POSITION);
                    String rankChangeText = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-rank-and-changes--change", Constants.FIRST_POSITION, "span", Constants.LAST_POSITION);
                    String detailLink = WebDriverUtils.findElementTextByClassAndAttr(element, "icon-link", Constants.LAST_POSITION, "href");

                    SaveAppRequestDTO.AppRequestDTO appRequestDTO = setAppRequestDTOBasicInfo(element, platformKey);

                    appRequestLinkMap.put(appRequestDTO, detailLink);
                    appRequestRankChangeMap.put(appRequestDTO, new RankingChange(NumberUtils.findIntFromString(newRankingText), NumberUtils.findIntFromString(rankChangeText)));
                } catch (Exception e) {
                    log.error("Failed to parse app request element", e);
                }
            }
        }
    }

    private SaveAppRequestDTO.AppRequestDTO setAppRequestDTOBasicInfo(Element element, String platformKey) {
        String appName = WebDriverUtils.findElementTextByClassAndTag(element, "app-link-container", Constants.LAST_POSITION, "span", Constants.LAST_POSITION);
        String seller = WebDriverUtils.findElementTextByClassAndTag(element, "company-info", Constants.LAST_POSITION, "span", Constants.LAST_POSITION);
        String ratingSinceFirstRelease = WebDriverUtils.findElementTextByClass(element, "rating-value", Constants.FIRST_POSITION);
        String ratingCountSinceFirstRelease = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-number", Constants.FIRST_POSITION, "span", Constants.LAST_POSITION);
        String ratingSinceCurrentRelease = WebDriverUtils.findElementTextByClass(element, "rating-value", Constants.LAST_POSITION);
        String ratingCountSinceCurrentRelease = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-number", Constants.LAST_POSITION, "span", Constants.LAST_POSITION);
        String firstRelease = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-date", Constants.FIRST_POSITION, "span", Constants.LAST_POSITION);
        String currentRelease = WebDriverUtils.findElementTextByClassAndTag(element, "tbl-col-date", Constants.LAST_POSITION, "span", Constants.LAST_POSITION);
        String storeAppId = WebDriverUtils.findElementTextByClassAndAttr(element, "tbl-col-app-v2", Constants.FIRST_POSITION, "data-appid");

        SaveAppRequestDTO.AppRequestDTO appRequestDTO = new SaveAppRequestDTO.AppRequestDTO();
        appRequestDTO.setName(appName);
        appRequestDTO.setSeller(seller);
        appRequestDTO.setPlatformKey(platformKey);
        appRequestDTO.setStoreAppId(storeAppId);
        appRequestDTO.setRatingSinceFirstRelease(parseDouble(ratingSinceFirstRelease));
        appRequestDTO.setRatingCountSinceFirstRelease(parseInt(ratingCountSinceFirstRelease));
        appRequestDTO.setRatingSinceCurrentRelease(parseDouble(ratingSinceCurrentRelease));
        appRequestDTO.setRatingCountSinceCurrentRelease(parseInt(ratingCountSinceCurrentRelease));
        appRequestDTO.setFirstReleasedAt(DateUtils.parseDateTime(firstRelease, "MMM dd, yyyy"));
        appRequestDTO.setCurrentReleasedAt(DateUtils.parseDateTime(currentRelease, "MMM dd, yyyy"));

        return appRequestDTO;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    private static class RankingChange {
        private Integer newRanking;
        private Integer rankChange;
    }

}
