package com.rockitgroup.infrastructure.data.warehouse.crawler;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.quartz.QuartzAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.File;
import java.io.IOException;

@Slf4j
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        QuartzAutoConfiguration.class,
})
@EnableAsync
@ImportResource({
        "classpath:spring/beanconfig/${" + AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME + "}/application-crawler-context.xml"
})
public class DataWarehouseCrawlerApplicationInitializer {

    public static void main(String[] args) throws IOException, InterruptedException {
        setupProperty();
        SpringApplication.run(DataWarehouseCrawlerApplicationInitializer.class, args);
    }


    private static void setupProperty() throws IOException {
        ClassPathResource chromeDriver = new ClassPathResource(Constants.CHROME_DRIVER_FILE_PATH);
        File tempFile = new File("/tmp/" + chromeDriver.getFilename());
        if (!tempFile.exists()) {
            if (!tempFile.createNewFile()) {
                throw new IOException("Cannot create new temp file for chromedriver");
            }
        }
        if (!tempFile.setExecutable(true)) {
            throw new IOException("Cannot set driver to be executable");
        }
        FileUtils.copyInputStreamToFile(chromeDriver.getInputStream(), tempFile);
        System.setProperty("webdriver.chrome.driver", tempFile.getAbsolutePath());
    }


}
