package com.rockitgroup.infrastructure.data.warehouse.crawler.service;

import com.rockitgroup.infrastructure.data.warehouse.crawler.service.impl.MediumPageExtractorImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PageExtractorFactory {

    @Autowired
    private MediumPageExtractorImpl mediumPageExtractorImpl;

    public PageExtractor getService(String siteKey) {
        if (siteKey.equals("MEDIUM")) {
            return mediumPageExtractorImpl;
        }

        return null;
    }
}
