package com.rockitgroup.infrastructure.data.warehouse.crawler.util;

import com.google.common.base.CharMatcher;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 10/5/18
 * Time: 2:17 AM
 */
public class NumberUtils {

    public static Double findDoubleFromString(String input) {
        if (input == null) {
            return 0.0;
        }
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return Double.parseDouble(matcher.group());
        }
        return 0.0;
    }

    public static Integer findIntFromString(String input) {
        if (input == null) {
            return 0;
        }
        String output = CharMatcher.digit().retainFrom(input);
        if (StringUtils.isNotEmpty(output)) {
            return Integer.parseInt(output);
        }
        return 0;
    }
}
