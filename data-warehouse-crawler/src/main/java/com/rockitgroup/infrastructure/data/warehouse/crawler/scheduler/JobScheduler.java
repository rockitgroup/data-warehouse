package com.rockitgroup.infrastructure.data.warehouse.crawler.scheduler;

import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobConfig;
import com.rockitgroup.infrastructure.data.warehouse.common.service.BatchJobConfigService;
import com.rockitgroup.infrastructure.data.warehouse.crawler.Constants;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.TimeZone;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Slf4j
@Component
public class JobScheduler {

    @Autowired
    private ApplicationContext applicationContext;

    @Getter
    private Scheduler scheduler;

    @PostConstruct
    public void postConstruct() throws IOException, SchedulerException {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();

        jobFactory.setApplicationContext(applicationContext);

        BatchJobConfigService batchJobConfigService = applicationContext.getBean(BatchJobConfigService.class);
        List<BatchJobConfig> batchJobConfigs = batchJobConfigService.findActiveBatchJobConfigs();

        StdSchedulerFactory factory = new StdSchedulerFactory();
        factory.initialize(new ClassPathResource(Constants.SCHEDULER_PROPERTIES_FILE_PATH).getInputStream());

        Scheduler scheduler = factory.getScheduler();
        scheduler.setJobFactory(jobFactory);

        for (BatchJobConfig batchJobConfig : batchJobConfigs) {
            try {
                Class clazz = Class.forName(batchJobConfig.getClassName());
                JobDetail jobDetail = createJobDetail(clazz, batchJobConfig.getJobName());
                Trigger trigger = createTrigger(jobDetail, batchJobConfig.getTrigger(), batchJobConfig.getSchedule());
                scheduler.scheduleJob(jobDetail, trigger);

                log.info(String.format("Successfully schedule job %s with schedule %s", batchJobConfig.getClassName(), batchJobConfig.getSchedule()));

            } catch (Exception e) {
                log.error(String.format("Failed to schedule job %s", batchJobConfig.getClassName()), e);
            }
        }


        scheduler.start();
        this.scheduler = scheduler;
    }

    public JobDetail createJobDetail(Class clazz, String jobName) {
        return newJob(clazz)
                .withIdentity(JobKey.jobKey(jobName))
                .build();
    }

    public Trigger createTrigger(JobDetail job, String jobTrigger, String jobSchedule) {
        return newTrigger()
                .forJob(job)
                .withIdentity(TriggerKey.triggerKey(jobTrigger))
                .withSchedule(cronSchedule(jobSchedule).inTimeZone(TimeZone.getDefault()))
                .build();
    }


}
