package com.rockitgroup.infrastructure.data.warehouse.crawler.scheduler.app;

import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.SiteTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.service.BatchJobTrackingService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.CrawlingEventService;
import com.rockitgroup.infrastructure.data.warehouse.common.service.OriginSiteService;
import com.rockitgroup.infrastructure.data.warehouse.crawler.exception.CaptchaException;
import com.rockitgroup.infrastructure.data.warehouse.crawler.service.PageExtractor;
import com.rockitgroup.infrastructure.data.warehouse.crawler.service.PageExtractorFactory;
import com.rockitgroup.infrastructure.data.warehouse.crawler.util.WebDriverUtils;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Stack;

@Slf4j
public class CrawlArticleHomePageJob implements Job {

    @Autowired
    private OriginSiteService originSiteService;

    @Autowired
    private PageExtractorFactory pageExtractorFactory;

    @Autowired
    private BatchJobTrackingService batchJobTrackingService;

    @Autowired
    private CrawlingEventService crawlingEventService;


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        BatchJobTracking batchJobTracking = BatchJobTracking.create(context.getJobDetail().getKey().getName());
        try {

            batchJobTrackingService.saveAsync(batchJobTracking);

            WebDriver driver = WebDriverUtils.getChromeDriver(true);

            List<OriginSite> originSites = originSiteService.findBySiteType(SiteTypeEnum.ARTICLE);
            for (OriginSite originSite : originSites) {
                PageExtractor pageExtractor = pageExtractorFactory.getService(originSite.getSiteKey());

                if (pageExtractor == null) {
                    continue;
                }

                pageExtractor.findListingPageUrlsFromHomePage(driver, originSite, batchJobTracking);
                pageExtractor.findDetailPageUrlsFromHomePage(driver, originSite, batchJobTracking);
            }

            driver.close();

            batchJobTracking.setResult(TrackingEventResultEnum.SUCCESS);

        } catch (CaptchaException e) {
            log.error("Captcha appear");
            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
            batchJobTracking.setOutput(e.getMessage());
        } catch (Exception e) {
            log.error("Failed to run crawlApp Job", e);
            batchJobTracking.setResult(TrackingEventResultEnum.FAIL);
            batchJobTracking.setOutput(e.getMessage());
        }

        batchJobTracking.setStatus(TrackingEventStatusEnum.DONE);
        batchJobTrackingService.saveAsync(batchJobTracking);
    }

}
