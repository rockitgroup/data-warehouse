#!/bin/bash

APP_NAME=identity-management
APP_SERVER=$APP_NAME-interfaces
JAVA=java
JAVA_OPTS="-Dfile.encoding=UTF-8 -Xmx1024m -Xms512M -XX:PermSize=256m -XX:MaxPermSize=256m -XX:MaxDirectMemorySize=1024M -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:-OmitStackTraceInFastThrow"
ROOT_DIR=/home/dtn1712
HOME=$ROOT_DIR/$APP_NAME
STAGE=prod
PID=$HOME/$APP_NAME.pid

export SPRING_PROFILES_ACTIVE=$STAGE
export GRADLE_OPTS="-Dorg.gradle.daemon=false"

source $ROOT_DIR/.bashrc

cd $HOME

# See how we were called.
case "$1" in
 start)
  git reset --hard HEAD && git pull origin $STAGE && ./gradlew :$APP_SERVER:clean
  ps -ef | grep $HOME | grep -v grep | awk '{print $2}' | xargs kill
  ./gradlew :$APP_SERVER:bootRun &
  echo $! > $NARUTO_PID
  ;;
 stop)
  ps -ef | grep $HOME | grep -v grep | awk '{print $2}' | xargs kill
  ;;
*)
 echo "Usage: $APP_NAME {start\|stop}";;
esac
exit 0