package com.rockitgroup.infrastructure.data.warehouse.common.model.job;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class BatchJobConfig extends ModifiableEntity {

    private String key;
    private String className;
    private String jobName;
    private String description;
    private boolean active;
    private String schedule;
    private String group;
    private String trigger;
}
