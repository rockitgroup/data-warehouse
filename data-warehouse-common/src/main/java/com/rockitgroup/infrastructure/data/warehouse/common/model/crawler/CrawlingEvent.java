package com.rockitgroup.infrastructure.data.warehouse.common.model.crawler;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.CrawlTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventStatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
public class CrawlingEvent extends ModifiableEntity {

    private DateTime crawlStartedAt;
    private DateTime crawlEndedAt;
    private String crawlUrl;

    @Enumerated(EnumType.STRING)
    private TrackingEventResultEnum result;

    @Enumerated(EnumType.STRING)
    private TrackingEventStatusEnum status;

    @Enumerated(EnumType.STRING)
    private CrawlTypeEnum crawlType;

    public static CrawlingEvent create(String crawlUrl, CrawlTypeEnum crawlType) {
        CrawlingEvent crawlingEvent = new CrawlingEvent();
        crawlingEvent.setCrawlStartedAt(DateTime.now());
        crawlingEvent.setCrawlUrl(crawlUrl);
        crawlingEvent.setCrawlType(crawlType);
        crawlingEvent.setStatus(TrackingEventStatusEnum.IN_PROGRESS);
        return crawlingEvent;
    }

    public static CrawlingEvent complete(CrawlingEvent crawlingEvent, TrackingEventResultEnum result) {
        crawlingEvent.setCrawlEndedAt(DateTime.now());
        crawlingEvent.setResult(result);
        crawlingEvent.setStatus(TrackingEventStatusEnum.DONE);
        return crawlingEvent;
    }
}
