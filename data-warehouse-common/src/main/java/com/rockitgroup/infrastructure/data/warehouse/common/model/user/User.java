package com.rockitgroup.infrastructure.data.warehouse.common.model.user;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.Sets;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Entity
@Cacheable
@ToString(of = { "name", "email" })
public class User extends Modifiable implements Serializable {
    public static final String EMPTY_USER = "--";
    private boolean active;

	private String name;

	@Column(unique = true)
	private String email;

	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Group> groups;

	protected User() {
	}

	public static User create(String name, String email) {
		User result = new User();
		result.name = name;
		result.email = email;
		result.groups = Sets.newHashSet();

		return result;
	}

    public static String getUserInfo(User user) {
        if (user == null) {
            return EMPTY_USER;
        }
        StringBuffer userInfo = new StringBuffer();
        if (user.getName() != null) {
            userInfo.append(user.getName());
        }
        if (user.getEmail() != null) {
            String email = user.getEmail();
            userInfo.append('(').append(email.substring(0, email.indexOf('@') > 0 ? email.indexOf('@') : email.length())).append(')');
        }
        return userInfo.toString();
    }

    public void setActive(boolean active) {
		this.active = active;
	}

	public void addGroup(Group group) {
		groups.add(group);
	}

	public void removeGroup(Group group) {
		groups.remove(group);
	}

	public List<AuthorityEnum> getAuthorities() {
		return FluentIterable.from(groups).filter(Group.FILTER_ACTIVE).transformAndConcat(Group.TO_AUTHORITIES).toList();
	}
}
