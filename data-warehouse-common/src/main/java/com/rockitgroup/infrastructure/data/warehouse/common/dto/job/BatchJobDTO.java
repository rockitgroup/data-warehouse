package com.rockitgroup.infrastructure.data.warehouse.common.dto.job;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BatchJobDTO {

    private String key;
    private String className;
    private String jobName;
    private String description;
    private boolean active;
    private String schedule;
    private String group;
    private String trigger;

}
