package com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.AppRankingTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class SaveAppRankingRequestDTO extends RequestDTO {

    List<AppRankingRequestDTO> appRankings;


    @Override
    public void validate() throws IllegalArgumentException {
        List<Long> failedAppIdList = appRankings.stream().filter(appRankingRequest -> !AppRankingTypeEnum.ALL_RANKING_TYPES_STR_SET.contains(appRankingRequest.getRankingType())).map(AppRankingRequestDTO::getAppId).collect(Collectors.toList());
        if (!failedAppIdList.isEmpty()) {
            throw new IllegalArgumentException("App id " + failedAppIdList.toString() + " does not have valid ranking type. Valid type is " + Arrays.toString(AppRankingTypeEnum.values()));
        }
    }

    @Getter
    @Setter
    public static class AppRankingRequestDTO {
        private Long appId;
        private String platformKey;
        private String category;
        private String country;
        private DateTime rankDate;
        private Integer ranking;
        private String rankingType;
    }
}
