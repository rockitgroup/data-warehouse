package com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration;

public enum  TrackingEventStatusEnum {
    PENDING, IN_PROGRESS, DONE
}
