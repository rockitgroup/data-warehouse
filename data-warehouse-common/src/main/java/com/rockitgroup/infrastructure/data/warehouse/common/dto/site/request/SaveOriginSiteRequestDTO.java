package com.rockitgroup.infrastructure.data.warehouse.common.dto.site.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class SaveOriginSiteRequestDTO extends RequestDTO {

    private String name;
    private String siteKey;
    private String url;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(name, "Missing parameter name");
        Assert.notNull(siteKey, "Missing parameter siteKey");
        Assert.notNull(url, "Missing parameter url");
    }
}
