package com.rockitgroup.infrastructure.data.warehouse.common.model.user;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import com.rockitgroup.infrastructure.vitamin.common.model.Modifiable;
import lombok.Getter;
import lombok.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Entity
@Cacheable
public class Group extends Modifiable implements Serializable {
	private String name;
	private boolean active;

	@ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
	private Set<User> users;

	@Enumerated(EnumType.STRING)
	@CollectionTable
	@Column
	@ElementCollection(targetClass = AuthorityEnum.class)
	private Set<AuthorityEnum> authorities;


	protected Group() {
	}

	public static Group create(String name) {
		Group result = new Group();
		result.name = name;
		result.active = true;
		result.authorities = Sets.newHashSet();
		result.users = Sets.newHashSet();
		return result;
	}

	public void clearAuthorities() {
		authorities.clear();
	}

	public static final Function TO_AUTHORITIES = new Function<Group, Set<AuthorityEnum>>() {
		@Override
		public Set<AuthorityEnum> apply(@NonNull Group input) {
			return input.getAuthorities();
		}
	};

	public static final Predicate<Group> FILTER_ACTIVE = new Predicate<Group>() {
		@Override
		public boolean apply(@NonNull Group input) {
			return input.isActive();
		}
	};

	public void setActive(boolean active) {
		this.active = active;
	}

	public void addAuthorities(Set<AuthorityEnum> authorities) {
		this.authorities.addAll(authorities);
	}

	public void addUsers(Set<User> users) {
		this.users.addAll(users);
	}

}
