package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobConfig;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.job.BatchJobConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class BatchJobConfigService extends DataWarehouseService {

    @Autowired
    private BatchJobConfigRepository batchJobConfigRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = batchJobConfigRepository;
    }

    public List<BatchJobConfig> findActiveBatchJobConfigs() {
        return batchJobConfigRepository.findActiveBatchJobConfigs();
    }

}
