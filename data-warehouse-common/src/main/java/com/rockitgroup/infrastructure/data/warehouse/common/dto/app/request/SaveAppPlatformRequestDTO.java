package com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

@Getter
@Setter
public class SaveAppPlatformRequestDTO extends RequestDTO {

    private String name;
    private String key;
    private String url;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(name, "Missing parameter name");
        Assert.notNull(key, "Missing parameter key");
        Assert.notNull(url, "Missing parameter url");
    }
}
