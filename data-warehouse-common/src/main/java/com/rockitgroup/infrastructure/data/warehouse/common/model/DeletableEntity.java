package com.rockitgroup.infrastructure.data.warehouse.common.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.MappedSuperclass;

@Getter
@Setter
@ToString(callSuper = true)
@MappedSuperclass
public class DeletableEntity extends ModifiableEntity {

    private boolean deleted;
}
