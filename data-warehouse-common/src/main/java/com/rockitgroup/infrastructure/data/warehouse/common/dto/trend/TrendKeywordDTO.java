package com.rockitgroup.infrastructure.data.warehouse.common.dto.trend;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrendKeywordDTO extends BaseDTO {
    private String keyword;
}
