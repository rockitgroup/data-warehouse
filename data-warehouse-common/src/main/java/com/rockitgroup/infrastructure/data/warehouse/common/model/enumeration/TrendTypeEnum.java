package com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/2/18
 * Time: 12:15 AM
 */
public enum TrendTypeEnum {

    DAILY,
    MONTHLY,
    REAL_TIME

}
