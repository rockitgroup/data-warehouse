package com.rockitgroup.infrastructure.data.warehouse.common.repository.job;

import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobConfig;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BatchJobConfigRepository extends BaseRepository<BatchJobConfig, Long> {


    @Query(value = "SELECT * FROM batch_job_configs WHERE active = 1", nativeQuery = true)
    List<BatchJobConfig> findActiveBatchJobConfigs();
}
