package com.rockitgroup.infrastructure.data.warehouse.common.dto.app;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class AppRankingChangeDTO extends BaseDTO {

    private String category;
    private String country;
    private AppDTO app;
    private Integer newRanking;
    private Integer rankChange;
    private DateTime rankDate;
    private AppPlatformDTO platform;
    private String rankingType;

}
