package com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;

@Getter
@Setter
public class SaveAppRankingChangeRequestDTO extends RequestDTO {

    private List<AppRankingChangeRequestDTO> appRankingChangeRequests;

    @Override
    public void validate() throws IllegalArgumentException {

    }

    @Getter
    @Setter
    public static class AppRankingChangeRequestDTO {
        private Long appId;
        private String platformKey;
        private String category;
        private String country;
        private DateTime rankDate;
        private Integer newRanking;
        private Integer rankChange;
        private String rankingType;
    }
}
