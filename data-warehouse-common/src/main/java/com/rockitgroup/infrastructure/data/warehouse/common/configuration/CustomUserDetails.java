package com.rockitgroup.infrastructure.data.warehouse.common.configuration;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.rockitgroup.infrastructure.data.warehouse.common.model.user.AuthorityEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.user.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomUserDetails implements UserDetails {
	private final User user;

	public CustomUserDetails(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Lists.transform(user.getAuthorities(), new Function<AuthorityEnum, GrantedAuthority>() {
			@Override
			public GrantedAuthority apply(AuthorityEnum input) {
				return new SimpleGrantedAuthority(input.name());
			}
		});
	}

	@Override
	public String getPassword() {
		return StringUtils.EMPTY;
	}

	@Override
	public String getUsername() {
		return user.getName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
