package com.rockitgroup.infrastructure.data.warehouse.common.dto.trend.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrendTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class SaveTrendRequestDTO extends RequestDTO{

    private String name;
    private Long categoryId;
    private String country;
    private Integer ranking;
    private String trendType;
    private Long originSiteId;
    private DateTime trendStartedAt;
    private DateTime trendEndedAt;
    private Integer volume;
    private List<String> trendKeywords;
    private List<String> trendReferenceUrls;

    @Override
    public void validate() throws IllegalArgumentException {
        Assert.notNull(name, "Missing parameter name");
        Assert.notNull(categoryId, "Missing parameter categoryId");
        Assert.notNull(originSiteId, "Missing parameter originSiteId");
        Assert.notNull(TrendTypeEnum.valueOf(trendType), "Missing/Invalid parameter for trendType. Valid trend type is: " + Arrays.toString(TrendTypeEnum.values()));
    }
}
