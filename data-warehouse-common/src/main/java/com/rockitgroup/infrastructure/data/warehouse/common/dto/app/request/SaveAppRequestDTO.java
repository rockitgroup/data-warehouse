package com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request;

import com.rockitgroup.infrastructure.vitamin.common.dto.RequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;

@Getter
@Setter
public class SaveAppRequestDTO extends RequestDTO {

    List<AppRequestDTO> apps;

    @Override
    public void validate() throws IllegalArgumentException {

    }

    @Getter
    @Setter
    public static class AppRequestDTO {
        private String name;
        private String description;
        private String url;
        private DateTime firstReleasedAt;
        private DateTime currentReleasedAt;
        private String size;
        private String platformKey;
        private String seller;
        private Double price;
        private Double ratingSinceFirstRelease;
        private Integer ratingCountSinceFirstRelease;
        private Double ratingSinceCurrentRelease;
        private Integer ratingCountSinceCurrentRelease;
        private String storeAppId;
    }
}
