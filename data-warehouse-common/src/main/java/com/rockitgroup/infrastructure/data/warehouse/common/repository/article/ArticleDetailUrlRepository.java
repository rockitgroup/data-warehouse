package com.rockitgroup.infrastructure.data.warehouse.common.repository.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleDetailUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleDetailUrlRepository extends BaseRepository<ArticleDetailUrl, Long> {

    @Query(value = "SELECT url FROM article_detail_urls WHERE url IN :urls", nativeQuery = true)
    List<String> findAllExistDetailUrls(@Param("urls") List<String> urls);

    @Query(value = "SELECT * FROM article_detail_urls WHERE originSiteId=:originSiteId AND status=:status LIMIT :limit", nativeQuery = true)
    List<ArticleDetailUrl> findAllByOriginSiteAndStatus(@Param("originSiteId") Long originSiteId, @Param("status") String status, @Param("limit") Integer limit);
}
