package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.repository.job.BatchJobTrackingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class BatchJobTrackingService extends DataWarehouseService {

    @Autowired
    private BatchJobTrackingRepository batchJobTrackingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = batchJobTrackingRepository;
    }
}
