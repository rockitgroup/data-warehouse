package com.rockitgroup.infrastructure.data.warehouse.common.dto.trend;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrendCategoryDTO extends BaseDTO {
    private String name;
    private String key;
}
