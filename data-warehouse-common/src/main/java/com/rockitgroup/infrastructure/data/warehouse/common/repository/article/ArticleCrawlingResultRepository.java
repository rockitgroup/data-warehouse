package com.rockitgroup.infrastructure.data.warehouse.common.repository.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleCrawlingResult;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleCrawlingResultRepository extends BaseRepository<ArticleCrawlingResult, Long> {

}
