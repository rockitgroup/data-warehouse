package com.rockitgroup.infrastructure.data.warehouse.common.util;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.Iterator;
import java.util.List;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 8/29/18
 * Time: 4:33 PM
 */
public class RepositoryUtils {

    public static String dotToCapitalized(String propertyName) {
        List<String> args = Lists.newArrayList(StringUtils.split(propertyName, "."));
        Iterator<String> argIterator = args.iterator();
        StringBuilder propertyNameBuilder = new StringBuilder(argIterator.next());
        while (argIterator.hasNext()) {
            propertyNameBuilder.append(StringUtils.capitalize(argIterator.next()));
        }
        return propertyNameBuilder.toString();
    }
}
