package com.rockitgroup.infrastructure.data.warehouse.common.model.site;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.net.URL;

@Getter
@ToString(exclude = { "originSite" })
@Entity
public class OriginSiteUrl extends ModifiableEntity {

	@ManyToOne
	@JoinColumn
	private OriginSite originSite;

	@NonNull
	@Column(unique = true)
	private URL url;

	public static OriginSiteUrl create(OriginSite targetSite, URL url) {
		OriginSiteUrl result = new OriginSiteUrl();
		result.originSite = targetSite;
		result.url = url;

		return result;
	}
}
