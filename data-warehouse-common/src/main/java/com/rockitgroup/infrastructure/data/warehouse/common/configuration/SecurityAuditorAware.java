package com.rockitgroup.infrastructure.data.warehouse.common.configuration;

import com.rockitgroup.infrastructure.data.warehouse.common.model.user.User;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class SecurityAuditorAware implements AuditorAware<User> {

	@Override
	public Optional<User> getCurrentAuditor() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null || !authentication.isAuthenticated()) {
			return Optional.empty();
		}

		if (authentication.getPrincipal() instanceof CustomUserDetails) {
			return Optional.of(((CustomUserDetails) authentication.getPrincipal()).getUser());
		}

		return Optional.empty();
	}

}
