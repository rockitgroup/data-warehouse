package com.rockitgroup.infrastructure.data.warehouse.common.repository.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleListingUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleListingUrlRepository extends BaseRepository<ArticleListingUrl, Long> {

    @Query(value = "SELECT url FROM article_listing_urls WHERE url IN :urls", nativeQuery = true)
    List<String> findAllExistListingUrls(@Param("urls") List<String> urls);

    List<ArticleListingUrl> findAllByOriginSiteAndStatus(OriginSite originSite, ArticleCrawlingUrlStatusEnum status);
}
