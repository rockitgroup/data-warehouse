package com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration;

import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 9/2/18
 * Time: 12:39 AM
 */
public enum AppRankingTypeEnum {

    FREE, PAID, GROSSING;

    public static final List<AppRankingTypeEnum> ALL_RANKING_TYPES = ImmutableList.of(FREE);

    public static final List<String> ALL_RANKING_TYPES_STR = ImmutableList.of(FREE.name());

    public static final Set<String> ALL_RANKING_TYPES_STR_SET = Collections.unmodifiableSet(
            new HashSet<>(ALL_RANKING_TYPES_STR));
}
