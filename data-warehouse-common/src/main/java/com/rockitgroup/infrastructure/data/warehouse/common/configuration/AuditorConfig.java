package com.rockitgroup.infrastructure.data.warehouse.common.configuration;

import com.rockitgroup.infrastructure.vitamin.common.configuration.BaseAuditorConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class AuditorConfig extends BaseAuditorConfig {

    @Override
    @Bean
    public SecurityAuditorAware auditorProvider() {
        return new SecurityAuditorAware();
    }
}
