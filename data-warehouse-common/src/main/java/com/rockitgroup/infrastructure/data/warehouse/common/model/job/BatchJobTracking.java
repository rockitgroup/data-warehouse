package com.rockitgroup.infrastructure.data.warehouse.common.model.job;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventResultEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrackingEventStatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Entity
public class BatchJobTracking extends ModifiableEntity {

    private String jobName;

    @Column(length = 65535,columnDefinition="Text")
    @Type(type="text")
    private String output;

    @Enumerated(EnumType.STRING)
    private TrackingEventStatusEnum status;

    @Enumerated(EnumType.STRING)
    private TrackingEventResultEnum result;

    public static BatchJobTracking create(String jobName) {
        BatchJobTracking batchJobTracking = new BatchJobTracking();
        batchJobTracking.setJobName(jobName);
        batchJobTracking.setStatus(TrackingEventStatusEnum.IN_PROGRESS);
        return batchJobTracking;
    }


}
