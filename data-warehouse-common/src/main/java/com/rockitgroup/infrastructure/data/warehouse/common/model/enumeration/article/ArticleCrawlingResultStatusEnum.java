package com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article;


public enum ArticleCrawlingResultStatusEnum {
    ACTIVE, HTML_PROCESSED, TRANSLATED
}
