package com.rockitgroup.infrastructure.data.warehouse.common.repository.user;

import com.rockitgroup.infrastructure.data.warehouse.common.model.user.Group;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends BaseRepository<Group, Long> {

    List<Group> findAllByActive(boolean active);

    Group findByName(String name);
}
