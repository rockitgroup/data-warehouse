package com.rockitgroup.infrastructure.data.warehouse.common.repository.site;

import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.SiteTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OriginSiteRepository extends BaseRepository<OriginSite, Long> {

    OriginSite findBySiteKey(String siteKey);

    List<OriginSite> findBySiteTypeAndActiveIsTrueAndCrawlingIsTrue(SiteTypeEnum siteType);
}
