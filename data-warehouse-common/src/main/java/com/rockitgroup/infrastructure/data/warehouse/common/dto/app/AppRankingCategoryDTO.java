package com.rockitgroup.infrastructure.data.warehouse.common.dto.app;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppRankingCategoryDTO extends BaseDTO {
    private String name;
    private String key;
}
