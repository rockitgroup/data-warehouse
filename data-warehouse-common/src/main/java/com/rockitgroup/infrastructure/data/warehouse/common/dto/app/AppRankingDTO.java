package com.rockitgroup.infrastructure.data.warehouse.common.dto.app;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class AppRankingDTO extends BaseDTO {
    private String category;
    private String country;
    private AppDTO app;
    private DateTime rankDate;
    private Integer ranking;
    private AppPlatformDTO platform;
    private String rankingType;
}
