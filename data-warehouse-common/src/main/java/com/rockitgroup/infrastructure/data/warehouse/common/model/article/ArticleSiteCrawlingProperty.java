package com.rockitgroup.infrastructure.data.warehouse.common.model.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter
@Setter
@Entity
public class ArticleSiteCrawlingProperty extends ModifiableEntity {

    @OneToOne
    @JoinColumn(name = "originSiteId")
    private OriginSite originSite;

    private String name;
    private String value;
    private String description;
    private boolean active;
}
