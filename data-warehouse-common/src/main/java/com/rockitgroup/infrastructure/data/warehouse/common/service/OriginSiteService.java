package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.SiteTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.site.OriginSiteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Slf4j
@Service
public class OriginSiteService extends DataWarehouseService {

    @Autowired
    private OriginSiteRepository originSiteRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = originSiteRepository;
    }

    public OriginSite getOrCreate(String name, String siteKey, String url) throws MalformedURLException {
        OriginSite originSite = originSiteRepository.findBySiteKey(siteKey);
        if (originSite == null) {
            originSite = OriginSite.create(name, siteKey, new URL(url));
            originSite = (OriginSite) save(originSite);
        }

        return originSite;
    }

    public List<OriginSite> findBySiteType(SiteTypeEnum siteTypeEnum) {
        return originSiteRepository.findBySiteTypeAndActiveIsTrueAndCrawlingIsTrue(siteTypeEnum);
    }
}
