package com.rockitgroup.infrastructure.data.warehouse.common.repository.crawler;

import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CrawlingEventRepository extends BaseRepository<CrawlingEvent, Long> {

}
