package com.rockitgroup.infrastructure.data.warehouse.common.repository.job;

import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchJobTrackingRepository extends BaseRepository<BatchJobTracking, Long> {

}
