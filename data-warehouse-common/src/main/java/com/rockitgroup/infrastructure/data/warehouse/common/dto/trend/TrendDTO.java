package com.rockitgroup.infrastructure.data.warehouse.common.dto.trend;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.site.OriginSiteDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import java.util.List;

@Getter
@Setter
public class TrendDTO extends BaseDTO {
    private String name;
    private TrendCategoryDTO category;
    private String country;
    private Integer ranking;
    private String trendType;
    private OriginSiteDTO originSite;
    private DateTime trendStartedAt;
    private DateTime trendEndedAt;
    private Integer volume;
    private List<TrendKeywordDTO> trendKeywords;
    private List<TrendReferenceUrlDTO> trendReferenceUrls;
}
