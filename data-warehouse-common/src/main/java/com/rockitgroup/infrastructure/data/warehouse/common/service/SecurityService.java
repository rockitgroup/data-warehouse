package com.rockitgroup.infrastructure.data.warehouse.common.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;

@Slf4j
@Service
public class SecurityService {

    private static final String AES_CIPHER_TYPE = "AES/CBC/PKCS5Padding";
    private static final String AES_KEY_SPEC = "AES";

    private static final int KEY_SIZE = 16;

    public byte[] decrypt(String secretKey, byte[] encryptData) throws Exception {
        if (encryptData == null || secretKey == null) {
            return null;
        }

        byte[] key = getBytesFromStringKey(secretKey);

        try {
            Cipher cipher = getCipher(AES_CIPHER_TYPE, DECRYPT_MODE, key, AES_KEY_SPEC);
            return cipher.doFinal(encryptData);
        } catch (Exception e) {
            log.error("Failed to decrypt", e);
        }

        return null;
    }

    public byte[] encrypt(String secretKey, byte[] rawData) throws Exception {
        if (rawData == null || secretKey == null) {
            return null;
        }

        byte[] key = getBytesFromStringKey(secretKey);

        try {
            Cipher cipher = getCipher(AES_CIPHER_TYPE, ENCRYPT_MODE, key, AES_KEY_SPEC);
            return cipher.doFinal(rawData);
        } catch (Exception e) {
            log.error("Failed to encrypt", e);
        }

        return null;
    }

    private Cipher getCipher(String cipherType, int mode, byte[] key, String keySpec) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance(cipherType);
        cipher.init(mode, new SecretKeySpec(key, keySpec), new IvParameterSpec(key));
        return cipher;
    }

    private byte[] getBytesFromStringKey(String secretKey) throws Exception {
        byte[] key = new byte[KEY_SIZE];
        byte[] secretKeyBytes = secretKey.getBytes();

        if (secretKeyBytes.length < KEY_SIZE) {
            throw new Exception("Secret Key does not have valid length");
        }

        System.arraycopy(secretKeyBytes, 0, key, 0, key.length);

        return key;
    }
}
