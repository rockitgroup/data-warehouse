package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleDetailUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.article.ArticleDetailUrlRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ArticleDetailUrlService extends DataWarehouseService {

    @Autowired
    private ArticleDetailUrlRepository articleDetailUrlRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = articleDetailUrlRepository;
    }

    public void batchCreateIfNotExist(List<ArticleDetailUrl> articleDetailUrls) {
        Map<String, ArticleDetailUrl> detailUrlMap = new HashMap<>();
        for (ArticleDetailUrl detailUrl : articleDetailUrls) {
            detailUrlMap.put(detailUrl.getUrl(), detailUrl);
        }

        List<String> newUrls = new ArrayList<>(detailUrlMap.keySet());
        List<ArticleDetailUrl> savedUrlItem = new ArrayList<>();
        List<String> existUrls = articleDetailUrlRepository.findAllExistDetailUrls(newUrls);

        newUrls.removeAll(existUrls);

        for (String url : newUrls) {
            if (detailUrlMap.containsKey(url)) {
                savedUrlItem.add(detailUrlMap.get(url));
            }
        }

        save(savedUrlItem);
    }

    public List<ArticleDetailUrl> findAllSiteActiveUrls(OriginSite originSite, Integer maxCrawlItem) {
        return articleDetailUrlRepository.findAllByOriginSiteAndStatus(originSite.getId(), ArticleCrawlingUrlStatusEnum.ACTIVE.name(), maxCrawlItem);
    }
}
