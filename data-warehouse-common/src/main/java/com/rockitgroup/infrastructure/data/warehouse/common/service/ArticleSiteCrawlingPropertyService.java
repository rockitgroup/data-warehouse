package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleSiteCrawlingProperty;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.article.ArticleSiteCrawlingPropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ArticleSiteCrawlingPropertyService extends DataWarehouseService {

    @Autowired
    private ArticleSiteCrawlingPropertyRepository articleSiteCrawlingPropertyRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = articleSiteCrawlingPropertyRepository;
    }

    public List<ArticleSiteCrawlingProperty> findAllPropertiesBySite(OriginSite originSite) {
        return articleSiteCrawlingPropertyRepository.findByOriginSiteAndActiveIsTrue(originSite);
    }

    public ArticleSiteCrawlingProperty findPropertyBySiteAndName(OriginSite originSite, String name) {
        return articleSiteCrawlingPropertyRepository.findOneByOriginSiteAndNameAndActiveIsTrue(originSite, name);
    }
}
