package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.model.DeletableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


public class DataWarehouseService {

    protected BaseRepository repository;

    @Transactional
    public Object save(ModifiableEntity object) {
        return repository.saveAndFlush(object);
    }

    @Transactional
    public List save(List objects) {
        return repository.saveAll(objects);
    }

    @Async
    @Transactional
    public void saveAsync(ModifiableEntity object) {
        repository.saveAndFlush(object);
    }

    @Async
    @Transactional
    public void saveAsync(List objects) {
        repository.saveAll(objects);
    }

    public void delete(DeletableEntity object) {
        object.setDeleted(true);
        save(object);
    }

    public Optional findOneById(Long id) {
        return repository.findById(id);
    }

    public List findAll() {
        return repository.findAll();
    }
}
