package com.rockitgroup.infrastructure.data.warehouse.common.dto.mapper;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.site.OriginSiteDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSiteUrl;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.MapperFacadeFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.net.URL;
import java.util.Collections;
import java.util.List;

@Component
public class OriginSiteDTOMappingHandler implements DTOMapper.MappingHandler<OriginSite, OriginSiteDTO> {

    @Override
    public OriginSiteDTO map(OriginSite model, Class<OriginSiteDTO> clazz) {
        OriginSiteDTO result = MapperFacadeFactory.create().map(model, OriginSiteDTO.class);
        List<OriginSiteUrl> originSiteUrls = MoreObjects.firstNonNull(model.getOriginSiteUrls(), Collections.<OriginSiteUrl>emptyList());
        List<URL> urls = Lists.transform(originSiteUrls, TO_URL);
        result.setUrls(urls);
        return result;
    }

    private static final Function<OriginSiteUrl, URL> TO_URL = new Function<OriginSiteUrl, URL>() {
        @Override
        public URL apply(@Nonnull OriginSiteUrl originSiteUrl) {
            return originSiteUrl.getUrl();
        }
    };
}