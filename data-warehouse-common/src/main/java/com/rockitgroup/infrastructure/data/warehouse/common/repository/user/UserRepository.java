package com.rockitgroup.infrastructure.data.warehouse.common.repository.user;

import com.rockitgroup.infrastructure.data.warehouse.common.model.user.AuthorityEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.user.User;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface UserRepository extends BaseRepository<User, Long> {

	User findOneById(long id);

	User findOneByEmail(String email);

	User findOneById(Long id);

	List<User> findByName(String name);

	Set<User> findByGroupsAuthorities(AuthorityEnum authorityEnum);

	List<User> findByEmailIn(Collection<String> email);
}
