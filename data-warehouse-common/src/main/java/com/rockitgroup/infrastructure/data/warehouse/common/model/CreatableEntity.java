package com.rockitgroup.infrastructure.data.warehouse.common.model;

import com.rockitgroup.infrastructure.data.warehouse.common.model.user.User;
import com.rockitgroup.infrastructure.vitamin.common.model.Creatable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Getter
@Setter(AccessLevel.PROTECTED)
@ToString(callSuper = true)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class CreatableEntity extends Creatable {

    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(updatable = false)
    private User createdBy;

	protected CreatableEntity() {
	}
}
