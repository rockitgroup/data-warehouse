package com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article;

/**
 * User: dtn1712 (dtn1712@coupang.com)
 * Date: 2019-04-25
 * Time: 21:08
 */
public enum ArticleCrawlingUrlStatusEnum {
    ACTIVE, CRAWLED, INACTIVE
}
