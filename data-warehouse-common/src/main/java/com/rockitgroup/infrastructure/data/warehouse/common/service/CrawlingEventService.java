package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.repository.crawler.CrawlingEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CrawlingEventService extends DataWarehouseService {

    @Autowired
    private CrawlingEventRepository crawlingEventRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = crawlingEventRepository;
    }
}
