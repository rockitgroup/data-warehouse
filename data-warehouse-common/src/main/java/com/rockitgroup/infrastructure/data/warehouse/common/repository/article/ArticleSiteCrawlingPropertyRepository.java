package com.rockitgroup.infrastructure.data.warehouse.common.repository.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleSiteCrawlingProperty;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleSiteCrawlingPropertyRepository extends BaseRepository<ArticleSiteCrawlingProperty, Long> {

    List<ArticleSiteCrawlingProperty> findByOriginSiteAndActiveIsTrue(OriginSite originSite);

    ArticleSiteCrawlingProperty findOneByOriginSiteAndNameAndActiveIsTrue(OriginSite originSite, String name);
}
