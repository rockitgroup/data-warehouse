package com.rockitgroup.infrastructure.data.warehouse.common.dto.site;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.List;

@Getter
@Setter
public class OriginSiteDTO extends BaseDTO {
    private String siteKey;
    private String name;
    private boolean crawling;
    private boolean active;
    private List<URL> urls;
}
