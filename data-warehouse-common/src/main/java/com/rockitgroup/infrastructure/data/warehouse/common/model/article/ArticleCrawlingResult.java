package com.rockitgroup.infrastructure.data.warehouse.common.model.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingResultStatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;

@Getter
@Setter
@Entity
public class ArticleCrawlingResult extends ModifiableEntity {

    private String url;
    private String title;
    private String mainImageUrl;

    @Lob
    @Type(type="text")
    private String originHtmlContent;

    private String publishedTime;
    private String readingTime;

    @Enumerated(EnumType.STRING)
    private ArticleCrawlingResultStatusEnum status;

    private String category;

}
