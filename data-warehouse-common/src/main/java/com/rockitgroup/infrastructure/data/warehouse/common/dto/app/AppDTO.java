package com.rockitgroup.infrastructure.data.warehouse.common.dto.app;

import com.rockitgroup.infrastructure.vitamin.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class AppDTO extends BaseDTO {
    private String name;
    private String description;
    private String url;
    private AppPlatformDTO platform;
    private DateTime firstReleasedAt;
    private DateTime currentReleasedAt;
    private String size;
    private String seller;
    private Double price;
    private Double ratingSinceFirstRelease;
    private Integer ratingCountSinceFirstRelease;
    private Double ratingSinceCurrentRelease;
    private Integer ratingCountSinceCurrentRelease;
    private String storeAppId;
}
