package com.rockitgroup.infrastructure.data.warehouse.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

@Slf4j
public class WebUtils extends org.springframework.web.util.WebUtils {


    private static final int maxConnectionPerRoute = 20;
    private static final int attemptNumber = 3;
    private static final int connectTimeOutInMS = 60000;
    private static final int readTimeOutInMS = 6000;
    private static final int requestTimeOutInMS = 6000;

    private static final String DEFAULT_CHARSET = "utf-8";

    private static final String USER_AGENT_HEADER_ATTR_NAME = "User-Agent";

    private static final String CONTENT_DISPOSITION_HEADER_ATTR_NAME = "Content-Disposition";
    private static final int DEFAULT_HTTP_PORT = 80;
    private static final int DEFAULT_HTTPS_PORT = 443;

    public static String getRequestURLWithQueryString(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String queryString = (request.getQueryString() != null) ? "?" + request.getQueryString() : "";
        return requestURI + queryString;
    }

    public static String getRequestAddress(HttpServletRequest request) {
        String requestAddress = request.getScheme() + "://" + request.getServerName()
                + ((request.getServerPort() == DEFAULT_HTTP_PORT || request.getServerPort() == DEFAULT_HTTPS_PORT) ? "" : ":" + request.getServerPort())
                + request.getContextPath();
        return requestAddress;
    }

    public static Cookie createCookie(String name, String value) {
        return createCookie(name, value, null, null, -1);
    }

    public static Cookie createCookie(String name, String value, int maxAge) {
        return createCookie(name, value, null, null, maxAge);
    }

    public static Cookie createCookie(String name, String value, String path, int maxAge) {
        return createCookie(name, value, null, path, maxAge);
    }

    public static Cookie createCookie(String name, String value, String domain, String path, int maxAge) {
        Cookie cookie = new Cookie(name, encode(value));

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (path != null) {
            cookie.setPath(path);
        }
        cookie.setMaxAge(maxAge);

        return cookie;
    }

    public static String getCookieValue(HttpServletRequest request, String name) {
        Cookie cookie = getCookie(request, name);
        return cookie != null ? decode(cookie.getValue()) : null;
    }

    public static boolean existCookie(HttpServletRequest request, String name) {
        return getCookie(request, name) != null;
    }

    public static String encode(String value) {
        if (value == null) {
            return null;
        }

        try {
            return URLEncoder.encode(value, DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public static String decode(String value) {
        try {
            return URLDecoder.decode(value, DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    public static void setDownloadFileName(HttpServletRequest request, HttpServletResponse response, String fileName) {
        String userAgent = request.getHeader(USER_AGENT_HEADER_ATTR_NAME);

        try {
            if (StringUtils.contains(userAgent, "MSIE")) {
                response.setHeader(CONTENT_DISPOSITION_HEADER_ATTR_NAME, "attachment;filename=" + URLEncoder.encode(fileName, DEFAULT_CHARSET) + ";");
            } else {
                response.setHeader(CONTENT_DISPOSITION_HEADER_ATTR_NAME, "attachment;filename="
                        + new String(fileName.getBytes(DEFAULT_CHARSET), "latin1") + ";");
            }

        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("파일명 지정 실패 - " + e.getMessage(), e);
        }

        response.setHeader("Pragma", "no-cache;");
        response.setHeader("Expires", "-1;");
    }

    public static HttpResponse get(String url)  {
        HttpGet request = new HttpGet(url);
        request.setHeader("Content-Type","application/json");
        HttpResponse response = null;
        try {
            HttpClient httpClient = createHttpClient();
            response = httpClient.execute(request);
        } catch(Exception e) {
            log.error("Unable to get from {}", (url), e);
        }  finally {
            request.releaseConnection();
        }

        return  response;
    }

    private static HttpClient createHttpClient() throws Exception {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(maxConnectionPerRoute);
        cm.setDefaultMaxPerRoute(maxConnectionPerRoute);

//        URL testSimilarityServerUrl = new URL(serverUrl);
//        HttpHost textSimilarityServerHost = new HttpHost(testSimilarityServerUrl.getHost(), testSimilarityServerUrl.getPort());
//        cm.setMaxPerRoute(new HttpRoute(textSimilarityServerHost), maxConnectionPerRoute);

        final DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(attemptNumber, true);
        final RequestConfig clientConfig = RequestConfig.copy(RequestConfig.DEFAULT)
                .setConnectTimeout(connectTimeOutInMS)
                .setSocketTimeout(readTimeOutInMS)
                .setConnectionRequestTimeout(requestTimeOutInMS)
                .build();

        return HttpClients.custom().setConnectionManager(cm)
                .setRetryHandler(retryHandler)
                .setDefaultRequestConfig(clientConfig)
                .build();
    }

}
