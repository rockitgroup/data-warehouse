package com.rockitgroup.infrastructure.data.warehouse.common.service;

import com.rockitgroup.infrastructure.data.warehouse.common.model.article.ArticleListingUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.article.ArticleListingUrlRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ArticleListingUrlService extends DataWarehouseService{

    @Autowired
    private ArticleListingUrlRepository articleListingUrlRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = articleListingUrlRepository;
    }

    public void batchCreateIfNotExist(List<ArticleListingUrl> articleListingUrls) {
        Map<String, ArticleListingUrl> listingUrlMap = new HashMap<>();
        for (ArticleListingUrl listingUrl : articleListingUrls) {
            listingUrlMap.put(listingUrl.getUrl(), listingUrl);
        }

        List<String> newUrls = new ArrayList<>(listingUrlMap.keySet());

        List<ArticleListingUrl> savedUrlItem = new ArrayList<>();
        List<String> existUrls = articleListingUrlRepository.findAllExistListingUrls(newUrls);

        newUrls.removeAll(existUrls);

        for (String url : newUrls) {
            if (listingUrlMap.containsKey(url)) {
                savedUrlItem.add(listingUrlMap.get(url));
            }
        }

        save(savedUrlItem);
    }

    public List<ArticleListingUrl> findAllSiteActiveUrls(OriginSite originSite) {
        return articleListingUrlRepository.findAllByOriginSiteAndStatus(originSite, ArticleCrawlingUrlStatusEnum.ACTIVE);
    }
}
