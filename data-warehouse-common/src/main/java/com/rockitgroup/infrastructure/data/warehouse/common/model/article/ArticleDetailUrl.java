package com.rockitgroup.infrastructure.data.warehouse.common.model.article;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.crawler.CrawlingEvent;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.article.ArticleCrawlingUrlStatusEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.job.BatchJobTracking;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class ArticleDetailUrl extends ModifiableEntity {

    private String url;

    private String originPostId;

    @Enumerated(EnumType.STRING)
    private ArticleCrawlingUrlStatusEnum status;

    @OneToOne
    private OriginSite originSite;

    @OneToOne
    @JoinColumn(name = "createdByJobId")
    private BatchJobTracking createdByJob;

    @OneToOne
    @JoinColumn(name = "lastCrawlingEventId")
    private CrawlingEvent lastCrawlingEvent;

    private String listingPageUrl;

    @OneToOne
    @JoinColumn(name = "crawlingResultId")
    private ArticleCrawlingResult crawlingResult;

}
