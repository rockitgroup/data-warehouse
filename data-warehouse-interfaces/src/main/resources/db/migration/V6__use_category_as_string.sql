ALTER TABLE `app_rankings`
  DROP COLUMN categoryId,
  ADD COLUMN `category` varchar(100) DEFAULT '';

ALTER TABLE `app_ranking_changes`
  DROP COLUMN categoryId,
  ADD COLUMN `category` varchar(100) DEFAULT '';