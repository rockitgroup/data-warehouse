CREATE TABLE `batch_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `className` varchar(300) NOT NULL,
  `jobName` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'active flag',
  `schedule` varchar(50) NOT NULL,
  `group` varchar(50) NOT NULL,
  `trigger` varchar(50) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;