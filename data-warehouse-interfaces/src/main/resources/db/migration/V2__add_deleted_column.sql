ALTER TABLE `trends`
  ADD COLUMN `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'deleted flag';

ALTER TABLE `trend_categories`
  ADD COLUMN `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'deleted flag';

ALTER TABLE `app_ranking_categories`
  ADD COLUMN `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'deleted flag';