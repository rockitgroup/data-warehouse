ALTER TABLE `app_rankings`
  ADD COLUMN `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'deleted flag';