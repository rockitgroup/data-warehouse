CREATE TABLE `app_ranking_changes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `categoryId` bigint(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `appId` bigint(20) NOT NULL,
  `rankDate` datetime NOT NULL,
  `rankChange` int(10) NOT NULL,
  `newRanking` int(10) NOT NULL,
  `platformId` bigint(20) NOT NULL,
  `rankingType` varchar(50) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'deleted flag',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;