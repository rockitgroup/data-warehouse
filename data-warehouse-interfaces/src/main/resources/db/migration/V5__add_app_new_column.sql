ALTER TABLE `apps`
  DROP COLUMN startedAt,
  DROP COLUMN review,
  DROP COLUMN reviewCount,
  ADD COLUMN `firstReleasedAt` datetime NOT NULL,
  ADD COLUMN `currentReleasedAt` datetime NOT NULL,
  ADD COLUMN `ratingSinceFirstRelease` double(10,2) NOT NULL,
  ADD COLUMN `ratingCountSinceFirstRelease` int(10) NOT NULL,
  ADD COLUMN `ratingSinceCurrentRelease` double(10,2) NOT NULL,
  ADD COLUMN `ratingCountSinceCurrentRelease` int(10) NOT NULL,
  MODIFY COLUMN `description` varchar(10000) DEFAULT '',
  MODIFY COLUMN `price` double(10,2) DEFAULT '0.0',
  MODIFY COLUMN `size` double(10,2) DEFAULT '0.0';

