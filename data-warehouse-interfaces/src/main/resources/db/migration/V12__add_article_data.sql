ALTER TABLE origin_sites
  ADD COLUMN `crawlType` varchar(100) NOT NULL COMMENT 'App, trend, deals, etc.';

DROP TABLE IF EXISTS `article_site_crawling_properties`;
CREATE TABLE `article_site_crawling_properties` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `originSiteId` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article_listing_urls`;
CREATE TABLE `article_listing_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE' COMMENT 'ACTIVE, INACTIVE',
  `originSiteId` bigint(20) NOT NULL,
  `createdByJobId` bigint(20) DEFAULT NULL,
  `lastCrawlingEventId` bigint(20) DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_listing_urls_unique01` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article_detail_urls`;
CREATE TABLE `article_detail_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `originPostId` varchar(255) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'ACTIVE' COMMENT 'ACTIVE, INACTIVE',
  `originSiteId` bigint(20) NOT NULL,
  `createdByJobId` bigint(20) DEFAULT NULL,
  `lastCrawlingEventId` bigint(20) DEFAULT NULL,
  `listingPageUrl` varchar(255) DEFAULT NULL,
  `crawlingResultId` bigint(20) DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `article_detail_urls_unique01` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `article_crawling_results`;
CREATE TABLE `article_crawling_results` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `mainImageUrl` varchar(255) DEFAULT NULL,
  `originHtmlContent` longtext NOT NULL,
  `publishedTime` varchar(50) DEFAULT NULL,
  `readingTime` varchar(30) DEFAULT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'PENDING' COMMENT 'PENDING, TRANSLATED, etc...',
  `category` varchar(255) DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
   PRIMARY KEY (`id`),
   UNIQUE KEY `article_crawling_results_unique01` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE origin_sites
  ADD COLUMN `homeUrl` varchar(255) NOT NULL;

ALTER TABLE batch_job_trackings
  MODIFY COLUMN `output` text DEFAULT NULL;

ALTER TABLE origin_sites
  CHANGE COLUMN `crawlType` `siteType` varchar(100) NOT NULL COMMENT 'App, trend, deals, etc.';