DROP TABLE IF EXISTS `group_authorities`;
CREATE TABLE `group_authorities` (
  `groupId` bigint(20) NOT NULL,
  `authorities` varchar(255) NOT NULL,
  PRIMARY KEY (`groupId`,`authorities`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `groupsId` bigint(20) NOT NULL,
  `usersId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupsId`,`usersId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ruleGroup` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_unique01` (`email`),
  KEY `users_idx01` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `crawling_events`;
CREATE TABLE `crawling_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `crawlStartedAt` datetime NOT NULL,
  `crawlEndedAt` datetime NOT NULL,
  `crawlUrl` VARCHAR(300) NOT NULL,
  `status` VARCHAR(50) NOT NULL,
  `result` VARCHAR(50) NOT NULL COMMENT 'Success or Fail',
  `crawlType` VARCHAR(100) NOT NULL COMMENT 'App, trend, deals, etc.',
  `objectId` bigint(20) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `trends`;
CREATE TABLE `trends` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `ranking` int(10) NOT NULL,
  `trendType` varchar(50) NOT NULL,
  `originSiteId` bigint(20) NOT NULL,
  `trendStartedAt` datetime NOT NULL,
  `trendEndedAt` datetime NOT NULL,
  `volume` int(10) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `trend_keywords`;
CREATE TABLE `trend_keywords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trendId` bigint(20) NOT NULL,
  `keyword` varchar(300) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `trend_reference_urls`;
CREATE TABLE `trend_reference_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trendId` bigint(20) NOT NULL,
  `url` varchar(1000) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `origin_sites`;
CREATE TABLE `origin_sites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `siteKey` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `crawling` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `origin_sites_unique01` (`name`),
  UNIQUE KEY `origin_sites_unique02` (`siteKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `origin_site_urls`;
CREATE TABLE `origin_site_urls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `originSiteId` bigint(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `origin_sites_urls_unique01` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `trend_categories`;
CREATE TABLE `trend_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trend_categories_unique01` (`name`),
  UNIQUE KEY `trend_categories_unique02` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `yearly_events`;
CREATE TABLE `yearly_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `eventDate` datetime NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` VARCHAR(300) NOT NULL DEFAULT '',
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `yearly_events_unique01` (`eventDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `app_ranking_categories`;
CREATE TABLE `app_ranking_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_categories_unique01` (`name`),
  UNIQUE KEY `app_categories_unique02` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `app_platforms`;
CREATE TABLE `app_platforms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  `url` varchar(300) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_platforms_unique01` (`name`),
  UNIQUE KEY `app_platforms_unique02` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `apps`;
CREATE TABLE `apps` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `startedAt` datetime NOT NULL,
  `url` varchar(300) NOT NULL,
  `size` int(10) NOT NULL COMMENT 'App size in Mb',
  `platformId` bigint(20) NOT NULL,
  `seller` varchar(100) NOT NULL,
  `price` double(10,2) NOT NULL,
  `review` double(10,2) NOT NULL,
  `reviewCount` int(10) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `app_rankings`;
CREATE TABLE `app_rankings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `categoryId` bigint(20) NOT NULL,
  `country` varchar(50) NOT NULL,
  `appId` bigint(20) NOT NULL,
  `rankDate` datetime NOT NULL,
  `ranking` int(10) NOT NULL,
  `platformId` bigint(20) NOT NULL,
  `rankingType` varchar(50) NOT NULL,
  `createdById` bigint(20) DEFAULT NULL,
  `updatedById` bigint(20) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;