package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrendReferenceUrl extends ModifiableEntity {

    @ManyToOne
    private Trend trend;

    @NonNull
    private String url;
}
