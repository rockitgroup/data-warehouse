package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app;

import com.rockitgroup.infrastructure.data.warehouse.common.model.DeletableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.AppRankingTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;


@Entity
@Getter
@Setter
public class AppRanking extends DeletableEntity {

    private String category;
    private String country;

    @OneToOne
    private App app;

    private DateTime rankDate;
    private Integer ranking;

    @OneToOne
    @JoinColumn(name = "platformId")
    private AppPlatform platform;

    @Enumerated(EnumType.STRING)
    private AppRankingTypeEnum rankingType;


}
