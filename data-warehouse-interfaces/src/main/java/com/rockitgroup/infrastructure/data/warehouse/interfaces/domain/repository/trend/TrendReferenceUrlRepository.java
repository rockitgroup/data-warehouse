package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendReferenceUrl;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrendReferenceUrlRepository extends BaseRepository<TrendReferenceUrl, Long> {
}
