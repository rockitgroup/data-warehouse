package com.rockitgroup.infrastructure.data.warehouse.interfaces.controller.site;


import com.rockitgroup.infrastructure.data.warehouse.common.dto.site.OriginSiteDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.site.request.SaveOriginSiteRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.service.OriginSiteService;
import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/originsite")
public class OriginSiteController extends BaseController {

    @Autowired
    private OriginSiteService originSiteService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseDTO save(@RequestBody @Valid SaveOriginSiteRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            requestDTO.validate();

            OriginSite originSite = originSiteService.getOrCreate(requestDTO.getName(), requestDTO.getSiteKey(), requestDTO.getUrl());
            return generateSuccessResponse(responseDTO, originSite, OriginSiteDTO.class);
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to save origin site", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }
}
