package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.common.Constants;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppRanking;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app.AppRankingRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import com.rockitgroup.infrastructure.data.warehouse.common.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class AppRankingService extends DataWarehouseService {

    @Autowired
    private AppRankingRepository appRankingRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = appRankingRepository;
    }

    public List<AppRanking> createOrUpdate(List<AppRanking> appRankings) {

        Set<Long> appIds = new HashSet<>();
        Set<Long> platformIds = new HashSet<>();
        Set<String> countries = new HashSet<>();
        Set<String> categories = new HashSet<>();
        Set<String> rankingDates = new HashSet<>();
        Set<String> rankingTypes = new HashSet<>();

        Map<String, AppRanking> appRankingMap = new HashMap<>();

        for (AppRanking appRanking : appRankings) {
            appIds.add(appRanking.getApp().getId());
            platformIds.add(appRanking.getPlatform().getId());
            countries.add(appRanking.getCountry());
            categories.add(appRanking.getCategory());
            rankingDates.add(DateUtils.formatDate(appRanking.getRankDate()));
            rankingTypes.add(appRanking.getRankingType().name());

            appRankingMap.put(getAppRankingMapTempKey(appRanking), appRanking);
        }

        List<AppRanking> existingAppRankings = appRankingRepository.findAllExistingAppRankings(
                new ArrayList<>(appIds), new ArrayList<>(countries), new ArrayList<>(categories),
                new ArrayList<>(rankingDates), new ArrayList<>(platformIds), new ArrayList<>(rankingTypes));

        if (existingAppRankings.isEmpty()) {
            return appRankingRepository.saveAll(appRankings);
        } else {
            List<AppRanking> createdAppRankings = new ArrayList<>();
            Set<String> existingAppRankingKeys = new HashSet<>();

            for (AppRanking existingAppRanking : existingAppRankings) {
                String key = getAppRankingMapTempKey(existingAppRanking);
                existingAppRankingKeys.add(key);
                if (appRankingMap.containsKey(key)) {
                    AppRanking newAppRanking = appRankingMap.get(key);
                    if (!Objects.equals(existingAppRanking.getRanking(), newAppRanking.getRanking())) {
                        existingAppRanking.setRanking(newAppRanking.getRanking());
                        createdAppRankings.add(existingAppRanking);
                    }
                }
            }

            for (AppRanking appRanking : appRankings) {
                String key = getAppRankingMapTempKey(appRanking);
                if (!existingAppRankingKeys.contains(key)) {
                    createdAppRankings.add(appRanking);
                }
            }

            return appRankingRepository.saveAll(createdAppRankings);
        }
    }

    public AppRanking createOrUpdate(AppRanking appRanking) {
        List<AppRanking> existingAppRankings = appRankingRepository.findAllExistingAppRankings(appRanking.getApp().getId(),
                appRanking.getCountry(), appRanking.getCategory(),
                appRanking.getRanking(), DateUtils.formatDate(appRanking.getRankDate()),
                appRanking.getPlatform().getId(), appRanking.getRankingType().name());

        if (!existingAppRankings.isEmpty()) {
            appRanking = existingAppRankings.remove(0);
            existingAppRankings.forEach(this::delete);
        }

        return (AppRanking) save(appRanking);
    }

    private String getAppRankingMapTempKey(AppRanking appRanking) {
        return Long.toString(appRanking.getApp().getId()) + Constants.CACHE_KEY_SEPARATOR
                + Long.toString(appRanking.getPlatform().getId()) + Constants.CACHE_KEY_SEPARATOR
                + appRanking.getCountry() + Constants.CACHE_KEY_SEPARATOR
                + appRanking.getCategory() + Constants.CACHE_KEY_SEPARATOR
                + DateUtils.formatDate(appRanking.getRankDate()) + Constants.CACHE_KEY_SEPARATOR
                + appRanking.getRankingType().name() + Constants.CACHE_KEY_SEPARATOR;
    }
}
