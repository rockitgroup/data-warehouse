package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.Trend;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendKeyword;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendReferenceUrl;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend.TrendRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import com.rockitgroup.infrastructure.data.warehouse.common.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TrendService extends DataWarehouseService {

    @Autowired
    private TrendRepository trendRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = trendRepository;
    }

    public Trend createOrUpdate(Trend trend, List<String> keywords, List<String> referenceUrls) {
        List<Trend> existingTrends = trendRepository.findAllExistingTrends(trend.getName(),
                trend.getCountry(), trend.getCategory().getId(),
                trend.getRanking(), trend.getOriginSite().getId(),
                trend.getVolume(), trend.getTrendType().name(), DateUtils.formatFullDateTime(trend.getTrendStartedAt()));

        if (existingTrends.isEmpty()) {
            trend.setTrendKeywords(getNewTrendKeywords(trend, keywords));
            trend.setTrendReferenceUrls(getNewTrendReferenceUrls(trend, referenceUrls));
        } else {
            trend = existingTrends.remove(0);
            existingTrends.forEach(this::delete);

            trend.getTrendKeywords().addAll(getSaveTrendKeywords(trend, keywords));
            trend.getTrendReferenceUrls().addAll(getSaveTrendReferenceUrls(trend, referenceUrls));
        }

        return (Trend) save(trend);
    }

    private List<TrendKeyword> getSaveTrendKeywords(Trend trend, List<String> newKeywords) {
        List<TrendKeyword> saveTrendKeywords = new ArrayList<>();
        Set<String> trendKeywords = trend.getTrendKeywordAsSet();
        saveTrendKeywords.addAll(newKeywords.stream().filter(keyword -> !trendKeywords.contains(keyword)).map(keyword -> new TrendKeyword(trend, keyword)).collect(Collectors.toList()));
        return saveTrendKeywords;
    }

    private List<TrendReferenceUrl> getSaveTrendReferenceUrls(Trend trend, List<String> newReferenceUrls) {
        List<TrendReferenceUrl> saveTrendReferenceUrls = new ArrayList<>();
        Set<String> trendReferenceUrls = trend.getTrendReferenceUrlsAsSet();
        saveTrendReferenceUrls.addAll(newReferenceUrls.stream().filter(referenceUrl -> !trendReferenceUrls.contains(referenceUrl)).map(referenceUrl -> new TrendReferenceUrl(trend, referenceUrl)).collect(Collectors.toList()));
        return saveTrendReferenceUrls;
    }

    private List<TrendReferenceUrl> getNewTrendReferenceUrls(Trend trend, List<String> trendReferenceUrls) {
        return trendReferenceUrls.stream().map(url -> new TrendReferenceUrl(trend, url)).collect(Collectors.toList());
    }

    private List<TrendKeyword> getNewTrendKeywords(Trend trend, List<String> trendKeywords) {
        return trendKeywords.stream().map(keyword -> new TrendKeyword(trend, keyword)).collect(Collectors.toList());
    }
}
