package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.App;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppRepository extends BaseRepository<App, Long> {

    @Query(value = "SELECT * FROM apps WHERE name IN :names AND platformId IN :platformIds", nativeQuery = true)
    List<App> findAllExistingAppsByNamesAndPlatforms(@Param("names") List<String> names, @Param("platformIds") List<Long> platformIds);

    @Query(value = "SELECT * FROM apps WHERE id IN :appIds", nativeQuery = true)
    List<App> findAllByIds(@Param("appIds") List<Long> appIds);
}
