package com.rockitgroup.infrastructure.data.warehouse.interfaces.controller.app;

import com.rockitgroup.infrastructure.data.warehouse.common.Constants;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppPlatformDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppRankingChangeDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.AppRankingDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppPlatformRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRankingChangeRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRankingRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.app.request.SaveAppRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.AppRankingTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.App;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppPlatform;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppRanking;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppRankingChange;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.AppPlatformService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.AppRankingChangeService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.AppRankingService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.AppService;
import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import com.rockitgroup.infrastructure.vitamin.common.dto.mapper.DTOMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/app")
public class AppController extends BaseController {

    @Autowired
    private AppService appService;

    @Autowired
    private AppPlatformService appPlatformService;

    @Autowired
    private AppRankingService appRankingService;

    @Autowired
    private AppRankingChangeService appRankingChangeService;

    @Autowired
    private DTOMapper dtoMapper;

    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveApp(@RequestBody SaveAppRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.validate();

            Map<String, AppPlatform> appPlatformMaps = findAllAppPlatforms();
            Set<String> appMap = new HashSet<>();

            List<App> apps = new ArrayList<>();
            Map<String, String> failed = new HashMap<>();
            for (SaveAppRequestDTO.AppRequestDTO appRequestDTO : requestDTO.getApps()) {
                String key = appRequestDTO.getName().trim() + Constants.CACHE_KEY_SEPARATOR + appRequestDTO.getPlatformKey();
                if (appPlatformMaps.containsKey(appRequestDTO.getPlatformKey())) {
                    if (!appMap.contains(key)) {
                        App app = new App();
                        app.setName(StringEscapeUtils.escapeJava(appRequestDTO.getName().trim()));
                        app.setStoreAppId(appRequestDTO.getStoreAppId());
                        app.setDescription(appRequestDTO.getDescription());
                        app.setPrice(appRequestDTO.getPrice());
                        app.setFirstReleasedAt(appRequestDTO.getFirstReleasedAt());
                        app.setCurrentReleasedAt(appRequestDTO.getCurrentReleasedAt());
                        app.setSeller(appRequestDTO.getSeller());
                        app.setSize(appRequestDTO.getSize());
                        app.setUrl(appRequestDTO.getUrl());
                        app.setPlatform(appPlatformMaps.get(appRequestDTO.getPlatformKey()));
                        app.setRatingSinceFirstRelease(appRequestDTO.getRatingSinceFirstRelease());
                        app.setRatingCountSinceFirstRelease(appRequestDTO.getRatingCountSinceFirstRelease());
                        app.setRatingSinceCurrentRelease(appRequestDTO.getRatingSinceCurrentRelease());
                        app.setRatingCountSinceCurrentRelease(appRequestDTO.getRatingCountSinceCurrentRelease());

                        apps.add(app);
                        appMap.add(key);
                    }
                } else {
                    failed.put(appRequestDTO.getName(), "Platform does not exist");
                }
            }

            if (failed.size() == requestDTO.getApps().size()) {
                return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), "All apps have error saving");
            }

            apps = appService.createOrUpdate(apps);

            Map<String, Object> result = new HashMap<>();
            result.put("failed", failed);
            result.put("saved", dtoMapper.map(apps, AppDTO.class));

            responseDTO = generateSuccessResponse(responseDTO);
            responseDTO.setObject(result);
            return responseDTO;
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to save app", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @RequestMapping(value = "/platform", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveAppPlatform(@RequestBody SaveAppPlatformRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.validate();

            AppPlatform appPlatform = new AppPlatform();
            appPlatform.setKey(requestDTO.getKey());
            appPlatform.setName(requestDTO.getName());
            appPlatform.setUrl(requestDTO.getUrl());

            appPlatform = appPlatformService.getOrCreate(appPlatform);

            return generateSuccessResponse(responseDTO, appPlatform, AppPlatformDTO.class);
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to save app platform", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @RequestMapping(value = "/ranking", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveAppRanking(@RequestBody SaveAppRankingRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.validate();

            Set<Long> appIds = requestDTO.getAppRankings().stream().map(SaveAppRankingRequestDTO.AppRankingRequestDTO::getAppId).collect(Collectors.toSet());

            Map<String, AppPlatform> appPlatformMap = findAllAppPlatforms();
            Map<Long, App> appMap = findApps(new ArrayList<>(appIds));
            List<AppRanking> appRankings = new ArrayList<>();
            Map<Long, String> failed = new HashMap<>();
            for (SaveAppRankingRequestDTO.AppRankingRequestDTO appRankingRequestDTO : requestDTO.getAppRankings()) {
                if (!appMap.containsKey(appRankingRequestDTO.getAppId())) {
                    failed.put(appRankingRequestDTO.getAppId(), "App Id does not exist");
                }

                if (!appPlatformMap.containsKey(appRankingRequestDTO.getPlatformKey())) {
                    failed.put(appRankingRequestDTO.getAppId(), "Platform does not exist");
                }

                if (!failed.containsKey(appRankingRequestDTO.getAppId())) {
                    AppRanking appRanking = new AppRanking();
                    appRanking.setApp(appMap.get(appRankingRequestDTO.getAppId()));
                    appRanking.setCategory(appRankingRequestDTO.getCategory());
                    appRanking.setCountry(appRankingRequestDTO.getCountry());
                    appRanking.setPlatform(appPlatformMap.get(appRankingRequestDTO.getPlatformKey()));
                    appRanking.setRankDate(appRankingRequestDTO.getRankDate());
                    appRanking.setRanking(appRankingRequestDTO.getRanking());
                    appRanking.setRankingType(AppRankingTypeEnum.valueOf(appRankingRequestDTO.getRankingType()));

                    appRankings.add(appRanking);
                }
            }

            appRankings = appRankingService.createOrUpdate(appRankings);

            Map<String, Object> result = new HashMap<>();
            result.put("failed", failed);
            result.put("saved", dtoMapper.map(appRankings, AppRankingDTO.class));

            responseDTO = generateSuccessResponse(responseDTO);
            responseDTO.setObject(result);
            return responseDTO;

        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to save app ranking", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    private Map<String, AppPlatform> findAllAppPlatforms() {
        List appPlatforms = appPlatformService.findAll();
        Map<String, AppPlatform> appPlatformMaps = new HashMap<>();
        for (Object element : appPlatforms) {
            AppPlatform appPlatform = (AppPlatform) element;
            appPlatformMaps.put(appPlatform.getKey(), appPlatform);
        }
        return appPlatformMaps;
    }


    private Map<Long, App> findApps(List<Long> appIds) {
        List<App> apps = appService.findAllByIds(appIds);
        Map<Long, App> result = new HashMap<>();
        for (App app : apps) {
            result.put(app.getId(), app);
        }
        return result;
    }

    @RequestMapping(value = "/ranking-change", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveAppRankingChange(@RequestBody SaveAppRankingChangeRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            requestDTO.validate();

            Set<Long> appIds = requestDTO.getAppRankingChangeRequests().stream().map(SaveAppRankingChangeRequestDTO.AppRankingChangeRequestDTO::getAppId).collect(Collectors.toSet());

            Map<String, AppPlatform> appPlatformMap = findAllAppPlatforms();
            Map<Long, App> appMap = findApps(new ArrayList<>(appIds));

            List<AppRankingChange> appRankingChanges = new ArrayList<>();
            Map<Long, String> failed = new HashMap<>();
            for (SaveAppRankingChangeRequestDTO.AppRankingChangeRequestDTO appRankingChangeRequestDTO : requestDTO.getAppRankingChangeRequests()) {
                if (!appMap.containsKey(appRankingChangeRequestDTO.getAppId())) {
                    failed.put(appRankingChangeRequestDTO.getAppId(), "App Id does not exist");
                }

                if (!appPlatformMap.containsKey(appRankingChangeRequestDTO.getPlatformKey())) {
                    failed.put(appRankingChangeRequestDTO.getAppId(), "Platform does not exist");
                }

                if (!failed.containsKey(appRankingChangeRequestDTO.getAppId())) {

                    AppRankingChange appRankingChange = new AppRankingChange();
                    appRankingChange.setApp(appMap.get(appRankingChangeRequestDTO.getAppId()));
                    appRankingChange.setCategory(appRankingChangeRequestDTO.getCategory());
                    appRankingChange.setCountry(appRankingChangeRequestDTO.getCountry());
                    appRankingChange.setPlatform(appPlatformMap.get(appRankingChangeRequestDTO.getPlatformKey()));
                    appRankingChange.setRankDate(appRankingChangeRequestDTO.getRankDate());
                    appRankingChange.setNewRanking(appRankingChangeRequestDTO.getNewRanking());
                    appRankingChange.setRankChange(appRankingChangeRequestDTO.getRankChange());
                    appRankingChange.setRankingType(AppRankingTypeEnum.valueOf(appRankingChangeRequestDTO.getRankingType()));

                    appRankingChanges.add(appRankingChange);
                }
            }

            appRankingChanges = appRankingChangeService.createOrUpdate(appRankingChanges);


            Map<String, Object> result = new HashMap<>();
            result.put("failed", failed);
            result.put("saved", dtoMapper.map(appRankingChanges, AppRankingChangeDTO.class));

            responseDTO = generateSuccessResponse(responseDTO);
            responseDTO.setObject(result);
            return responseDTO;
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to save app ranking change", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @RequestMapping(value = "/platform/list", method = RequestMethod.GET)
    public ResponseDTO listAppPlatforms() {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            List<AppPlatform> appPlatforms = appPlatformService.findAll();
            return generateSuccessResponse(responseDTO, appPlatforms, AppPlatformDTO.class);
        } catch (Exception e) {
            log.error("Failed to list app platform", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

}
