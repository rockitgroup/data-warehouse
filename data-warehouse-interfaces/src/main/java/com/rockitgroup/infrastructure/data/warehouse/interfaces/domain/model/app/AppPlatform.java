package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;


@Entity
@Getter
@Setter
public class AppPlatform extends ModifiableEntity {

    private String name;
    private String key;
    private String url;

}
