package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app;

import com.rockitgroup.infrastructure.data.warehouse.common.model.DeletableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.AppRankingTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class AppRankingChange extends DeletableEntity {

    private String category;

    private String country;

    @OneToOne
    private App app;

    private Integer newRanking;
    private Integer rankChange;

    private DateTime rankDate;

    @OneToOne
    @JoinColumn(name = "platformId")
    private AppPlatform platform;

    @Enumerated(EnumType.STRING)
    private AppRankingTypeEnum rankingType;
}
