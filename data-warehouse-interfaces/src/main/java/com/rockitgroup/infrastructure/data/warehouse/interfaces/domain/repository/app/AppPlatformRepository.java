package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppPlatform;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppPlatformRepository extends BaseRepository<AppPlatform, Long> {

    AppPlatform findByKey(String key);

    @Query(value = "SELECT * FROM app_platforms WHERE keys IN :keys", nativeQuery = true)
    List<AppPlatform> findAllByKeys(@Param("keys") List<String> keys);
}
