package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendCategory;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrendCategoryRepository extends BaseRepository<TrendCategory, Long> {

    TrendCategory findByKeyAndDeleted(String key, boolean deleted);
}
