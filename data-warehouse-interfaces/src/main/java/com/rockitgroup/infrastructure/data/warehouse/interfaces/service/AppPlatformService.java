package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppPlatform;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app.AppPlatformRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class AppPlatformService extends DataWarehouseService {

    @Autowired
    private AppPlatformRepository appPlatformRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = appPlatformRepository;
    }

    public AppPlatform getOrCreate(AppPlatform input) {
        AppPlatform appPlatform = appPlatformRepository.findByKey(input.getKey());
        if (appPlatform == null) {
            appPlatform = (AppPlatform) save(input);
        }
        return appPlatform;
    }

    public AppPlatform findOneByKey(String key) {
        return appPlatformRepository.findByKey(key);
    }

    public List<AppPlatform> findAllByKeys(List<String> keys) {
        return appPlatformRepository.findAllByKeys(keys);
    }


}
