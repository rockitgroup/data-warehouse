package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppRanking;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppRankingRepository extends BaseRepository<AppRanking, Long> {

    @Query(value = "SELECT * FROM app_rankings " +
            "WHERE appId IN :appIds " +
            "AND country IN :countries " +
            "AND category IN :categories " +
            "AND rankDate IN :rankDates " +
            "AND platformId IN :platformIds " +
            "AND rankingType IN :rankingTypes", nativeQuery = true)
    List<AppRanking> findAllExistingAppRankings(@Param("appIds") List<Long> appIds,
                                                @Param("countries") List<String> countries,
                                                @Param("categories") List<String> categories,
                                                @Param("rankDates") List<String> rankDates,
                                                @Param("platformIds") List<Long> platformIds,
                                                @Param("rankingTypes") List<String> rankingTypes);


    @Query(value = "SELECT * FROM app_rankings " +
            "WHERE appId=:appId " +
            "AND country=:country " +
            "AND category=:category " +
            "AND ranking=:ranking " +
            "AND rankDate=:rankDate " +
            "AND platformId=:platformId " +
            "AND rankingType=:rankingType", nativeQuery = true)
    List<AppRanking> findAllExistingAppRankings(@Param("appId") Long appId,
                                                @Param("country") String country,
                                                @Param("category") String category,
                                                @Param("ranking") Integer ranking,
                                                @Param("rankDate") String rankDate,
                                                @Param("platformId") Long platformId,
                                                @Param("rankingType") String rankingType);
}
