package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendCategory;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend.TrendCategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class TrendCategoryService extends DataWarehouseService {

    @Autowired
    private TrendCategoryRepository trendCategoryRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = trendCategoryRepository;
    }

    public TrendCategory getOrCreate(TrendCategory input) {
        TrendCategory trendCategory = trendCategoryRepository.findByKeyAndDeleted(input.getKey(), false);
        if (trendCategory == null) {
            trendCategory = (TrendCategory) save(input);
        }
        return trendCategory;
    }
}
