package com.rockitgroup.infrastructure.data.warehouse.interfaces.controller.trend;


import com.rockitgroup.infrastructure.data.warehouse.common.dto.trend.TrendCategoryDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.trend.TrendDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.trend.request.SaveTrendCategoryRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.dto.trend.request.SaveTrendRequestDTO;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrendTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import com.rockitgroup.infrastructure.data.warehouse.common.service.OriginSiteService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.Trend;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendCategory;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.TrendCategoryService;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.service.TrendService;
import com.rockitgroup.infrastructure.vitamin.common.controller.BaseController;
import com.rockitgroup.infrastructure.vitamin.common.dto.ResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1/trend")
public class TrendController extends BaseController {

    @Autowired
    private TrendService trendService;

    @Autowired
    private TrendCategoryService trendCategoryService;

    @Autowired
    private OriginSiteService originSiteService;

    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveTrend(@RequestBody @Valid SaveTrendRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            requestDTO.validate();

            Optional trendCategory = trendCategoryService.findOneById(requestDTO.getCategoryId());
            if (!trendCategory.isPresent()) {
                return generateFailResponse(responseDTO, HttpStatus.NOT_FOUND.value(), "Category Id does not exist");
            }

            Optional originSite = originSiteService.findOneById(requestDTO.getOriginSiteId());
            if (!originSite.isPresent()) {
                return generateFailResponse(responseDTO, HttpStatus.NOT_FOUND.value(), "Origin Site Id does not exist");
            }

            Trend trend = new Trend();
            trend.setCategory((TrendCategory) trendCategory.get());
            trend.setCountry(requestDTO.getCountry());
            trend.setName(requestDTO.getName());
            trend.setOriginSite((OriginSite) originSite.get());
            trend.setRanking(requestDTO.getRanking());
            trend.setTrendStartedAt(requestDTO.getTrendStartedAt());
            trend.setTrendEndedAt(requestDTO.getTrendEndedAt());
            trend.setTrendType(TrendTypeEnum.valueOf(requestDTO.getTrendType()));
            trend.setVolume(requestDTO.getVolume());

            trend = trendService.createOrUpdate(trend, requestDTO.getTrendKeywords(), requestDTO.getTrendReferenceUrls());

            return generateSuccessResponse(responseDTO, trend, TrendDTO.class);
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to create trend", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

    @RequestMapping(value = "/category", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseDTO saveTrendCategory(@RequestBody @Valid SaveTrendCategoryRequestDTO requestDTO) {
        ResponseDTO responseDTO = new ResponseDTO();

        try {
            requestDTO.validate();

            TrendCategory trendCategory = trendCategoryService.getOrCreate(new TrendCategory(requestDTO.getName(), requestDTO.getKey()));

            return generateSuccessResponse(responseDTO, trendCategory, TrendCategoryDTO.class);
        } catch (IllegalArgumentException e) {
            log.error("Request input is invalid", e);
            return generateFailResponse(responseDTO, HttpStatus.BAD_REQUEST.value(), e.getMessage());
        } catch (Exception e) {
            log.error("Failed to create trend category", e);
            return generateFailResponse(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        }
    }

}
