package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.common.Constants;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.AppRankingChange;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app.AppRankingChangeRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import com.rockitgroup.infrastructure.data.warehouse.common.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class AppRankingChangeService extends DataWarehouseService {

    @Autowired
    private AppRankingChangeRepository appRankingChangeRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = appRankingChangeRepository;
    }


    public List<AppRankingChange> createOrUpdate(List<AppRankingChange> appRankingChanges) {

        Set<Long> appIds = new HashSet<>();
        Set<Long> platformIds = new HashSet<>();
        Set<String> countries = new HashSet<>();
        Set<String> categories = new HashSet<>();
        Set<String> rankingDates = new HashSet<>();
        Set<String> rankingTypes = new HashSet<>();

        Map<String, AppRankingChange> appRankingChangeMap = new HashMap<>();

        for (AppRankingChange appRankingChange : appRankingChanges) {
            appIds.add(appRankingChange.getApp().getId());
            platformIds.add(appRankingChange.getPlatform().getId());
            countries.add(appRankingChange.getCountry());
            categories.add(appRankingChange.getCategory());
            rankingDates.add(DateUtils.formatDate(appRankingChange.getRankDate()));
            rankingTypes.add(appRankingChange.getRankingType().name());

            appRankingChangeMap.put(getAppRankingChangeMapTempKey(appRankingChange), appRankingChange);
        }

        List<AppRankingChange> existingAppRankingChanges = appRankingChangeRepository.findAllExistingAppRankingChanges(
                new ArrayList<>(appIds), new ArrayList<>(countries), new ArrayList<>(categories),
                new ArrayList<>(rankingDates), new ArrayList<>(platformIds), new ArrayList<>(rankingTypes));

        if (existingAppRankingChanges.isEmpty()) {
            return appRankingChangeRepository.saveAll(appRankingChanges);
        } else {
            List<AppRankingChange> createdAppRankingChanges = new ArrayList<>();
            Set<String> existingAppRankingChangeKeys = new HashSet<>();

            for (AppRankingChange existingAppRankingChange : existingAppRankingChanges) {
                String key = getAppRankingChangeMapTempKey(existingAppRankingChange);
                existingAppRankingChangeKeys.add(key);

                if (appRankingChangeMap.containsKey(key)) {
                    AppRankingChange newAppRankingChange = appRankingChangeMap.get(key);
                    if (!Objects.equals(existingAppRankingChange.getNewRanking(), newAppRankingChange.getNewRanking()) ||
                            !Objects.equals(existingAppRankingChange.getRankChange(), newAppRankingChange.getRankChange())) {

                        existingAppRankingChange.setNewRanking(newAppRankingChange.getNewRanking());
                        existingAppRankingChange.setRankChange(newAppRankingChange.getRankChange());
                        createdAppRankingChanges.add(existingAppRankingChange);
                    }
                }
            }

            for (AppRankingChange appRankingChange : appRankingChanges) {
                String key = getAppRankingChangeMapTempKey(appRankingChange);
                if (!existingAppRankingChangeKeys.contains(key)) {
                    createdAppRankingChanges.add(appRankingChange);
                }
            }

            return appRankingChangeRepository.saveAll(createdAppRankingChanges);
        }
    }


    public AppRankingChange createOrUpdate(AppRankingChange appRankingChange) {
        List<AppRankingChange> existingAppRankingChanges = appRankingChangeRepository.findAllExistingAppRankingChanges(appRankingChange.getApp().getId(),
                appRankingChange.getCountry(), appRankingChange.getCategory(),
                appRankingChange.getNewRanking(), appRankingChange.getRankChange(), DateUtils.formatDate(appRankingChange.getRankDate()),
                appRankingChange.getPlatform().getId(), appRankingChange.getRankingType().name());

        if (!existingAppRankingChanges.isEmpty()) {
            appRankingChange = existingAppRankingChanges.remove(0);
            existingAppRankingChanges.forEach(this::delete);
        }

        return (AppRankingChange) save(appRankingChange);
    }


    private String getAppRankingChangeMapTempKey(AppRankingChange appRankingChange) {
        return Long.toString(appRankingChange.getApp().getId()) + Constants.CACHE_KEY_SEPARATOR
                + Long.toString(appRankingChange.getPlatform().getId()) + Constants.CACHE_KEY_SEPARATOR
                + appRankingChange.getCountry() + Constants.CACHE_KEY_SEPARATOR
                + appRankingChange.getCategory() + Constants.CACHE_KEY_SEPARATOR
                + DateUtils.formatDate(appRankingChange.getRankDate()) + Constants.CACHE_KEY_SEPARATOR
                + appRankingChange.getRankingType().name() + Constants.CACHE_KEY_SEPARATOR;
    }
}
