package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend;

import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.TrendKeyword;
import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrendKeywordRepository extends BaseRepository<TrendKeyword, Long> {

    @Query(value = "SELECT * FROM trend_keywords " +
            "WHERE trendId=:trendId " +
            "AND keyword IN :keyword", nativeQuery = true)
    List<TrendKeyword> findAllByTrendAndKeywords(@Param("trendId") Long trendId, @Param("keyword") List<String> keyword);

}
