package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend;

import com.rockitgroup.infrastructure.data.warehouse.common.model.DeletableEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TrendCategory extends DeletableEntity {

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String key;

}
