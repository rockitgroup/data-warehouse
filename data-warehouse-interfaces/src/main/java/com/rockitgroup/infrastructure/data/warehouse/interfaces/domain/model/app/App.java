package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app;

import com.rockitgroup.infrastructure.data.warehouse.common.model.ModifiableEntity;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class App extends ModifiableEntity{

    private String name;
    private String description;
    private String url;
    private DateTime firstReleasedAt;
    private DateTime currentReleasedAt;
    private String size;

    private String storeAppId;

    @OneToOne
    @JoinColumn(name = "platformId")
    private AppPlatform platform;

    private String seller;

    private Double price;
    private Double ratingSinceFirstRelease;
    private Integer ratingCountSinceFirstRelease;
    private Double ratingSinceCurrentRelease;
    private Integer ratingCountSinceCurrentRelease;

}
