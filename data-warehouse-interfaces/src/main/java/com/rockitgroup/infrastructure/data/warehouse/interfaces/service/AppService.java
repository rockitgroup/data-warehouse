package com.rockitgroup.infrastructure.data.warehouse.interfaces.service;

import com.rockitgroup.infrastructure.data.warehouse.common.Constants;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.app.App;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.app.AppRepository;
import com.rockitgroup.infrastructure.data.warehouse.common.service.DataWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class AppService extends DataWarehouseService {

    @Autowired
    private AppRepository appRepository;

    @PostConstruct
    public void postConstruct() {
        this.repository = appRepository;
    }

    public List<App> findAllByIds(List<Long> appIds) {
        return appRepository.findAllByIds(appIds);
    }

    public List<App> createOrUpdate(List<App> apps) {
        Set<String> appNames = new HashSet<>();
        Set<Long> appPlatformIdList = new HashSet<>();

        Map<String, App> appMap = new HashMap<>();
        for (App app : apps) {
            appNames.add(app.getName());
            appPlatformIdList.add(app.getPlatform().getId());
            appMap.put(app.getName() + Constants.CACHE_KEY_SEPARATOR + Long.toString(app.getPlatform().getId()), app);
        }

        List<App> existingApps = appRepository.findAllExistingAppsByNamesAndPlatforms(new ArrayList<>(appNames), new ArrayList<>(appPlatformIdList));

        if (existingApps.isEmpty()) {
            return appRepository.saveAll(apps);
        } else {
            List<App> createdApps = new ArrayList<>();
            List<App> updatedApps = new ArrayList<>();
            Set<String> updatedAppKeys = new HashSet<>();
            for (App existingApp : existingApps) {
                String key = existingApp.getName() + Constants.CACHE_KEY_SEPARATOR + Long.toString(existingApp.getPlatform().getId());
                if (appMap.containsKey(key)) {
                    App newApp = appMap.get(key);
                    existingApp.setDescription(newApp.getDescription());
                    existingApp.setPrice(newApp.getPrice());
                    existingApp.setStoreAppId(newApp.getStoreAppId());
                    existingApp.setFirstReleasedAt(newApp.getFirstReleasedAt());
                    existingApp.setCurrentReleasedAt(newApp.getCurrentReleasedAt());
                    existingApp.setSeller(newApp.getSeller());
                    existingApp.setSize(newApp.getSize());
                    existingApp.setUrl(newApp.getUrl());
                    existingApp.setRatingSinceFirstRelease(newApp.getRatingSinceFirstRelease());
                    existingApp.setRatingCountSinceFirstRelease(newApp.getRatingCountSinceFirstRelease());
                    existingApp.setRatingSinceCurrentRelease(newApp.getRatingSinceCurrentRelease());
                    existingApp.setRatingCountSinceCurrentRelease(newApp.getRatingCountSinceCurrentRelease());

                    updatedApps.add(existingApp);
                    updatedAppKeys.add(key);
                }
            }

            for (App app : apps) {
                String key = app.getName() + Constants.CACHE_KEY_SEPARATOR + Long.toString(app.getPlatform().getId());
                if (!updatedAppKeys.contains(key)) {
                    createdApps.add(app);
                }
            }

            updatedApps = appRepository.saveAll(updatedApps);
            createdApps = appRepository.saveAll(createdApps);
            createdApps.addAll(updatedApps);
            return createdApps;
        }

    }

}
