package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend;

import com.google.common.collect.Sets;
import com.rockitgroup.infrastructure.data.warehouse.common.model.DeletableEntity;
import com.rockitgroup.infrastructure.data.warehouse.common.model.enumeration.TrendTypeEnum;
import com.rockitgroup.infrastructure.data.warehouse.common.model.site.OriginSite;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Trend extends DeletableEntity {

    private String name;

    @OneToOne
    @JoinColumn(name = "categoryId")
    private TrendCategory category;

    private String country;
    private Integer ranking;

    @Enumerated(EnumType.STRING)
    private TrendTypeEnum trendType;

    @OneToOne
    private OriginSite originSite;

    private DateTime trendStartedAt;
    private DateTime trendEndedAt;
    private Integer volume;

    @Setter
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trend", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<TrendKeyword> trendKeywords;

    @Setter
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trend", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<TrendReferenceUrl> trendReferenceUrls;

    public Set<String> getTrendKeywordAsSet() {
        return (trendKeywords == null) ? Sets.newHashSet() :
                trendKeywords.stream().map(TrendKeyword::getKeyword).collect(Collectors.toSet());
    }

    public Set<String> getTrendReferenceUrlsAsSet() {
        return (trendReferenceUrls == null) ? Sets.newHashSet() :
                trendReferenceUrls.stream().map(TrendReferenceUrl::getUrl).collect(Collectors.toSet());
    }
}
