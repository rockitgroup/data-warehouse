package com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.repository.trend;

import com.rockitgroup.infrastructure.data.warehouse.common.repository.BaseRepository;
import com.rockitgroup.infrastructure.data.warehouse.interfaces.domain.model.trend.Trend;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrendRepository extends BaseRepository<Trend, Long> {

    @Query(value = "SELECT * FROM trends " +
            "WHERE name=:name " +
            "AND country=:country " +
            "AND categoryId=:categoryId " +
            "AND ranking=:ranking " +
            "AND originSiteId=:originSiteId " +
            "AND volume=:volume " +
            "AND trendType=:trendType " +
            "AND trendStartedAt <= :trendStartedAt AND trendEndedAt >= :trendStartedAt " +
            "AND deleted = 0", nativeQuery = true)
    List<Trend> findAllExistingTrends(@Param("name") String name,
                                      @Param("country") String country,
                                      @Param("categoryId") Long categoryId,
                                      @Param("ranking") Integer ranking,
                                      @Param("originSiteId") Long originSiteId,
                                      @Param("volume") Integer volume,
                                      @Param("trendType") String trendType,
                                      @Param("trendStartedAt") String trendStartedAt);
}
