#!/bin/bash

APP_NAME=data-warehouse
APP_SERVER=$APP_NAME-interfaces
JAVA=java
JAVA_OPTS="-Dfile.encoding=UTF-8 -Xmx512m -Xms256M -XX:PermSize=128m -XX:MaxPermSize=128m -XX:MaxDirectMemorySize=512M -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:-OmitStackTraceInFastThrow"
ROOT_DIR=/Users/dangnguyen/Work/RIG/Development/infrastructure
HOME=$ROOT_DIR/$APP_NAME
STAGE=local
BRANCH=master
PID=$HOME/$APP_SERVER.pid

export SPRING_PROFILES_ACTIVE=$STAGE
export GRADLE_OPTS="-Dorg.gradle.daemon=false"

#source $ROOT_DIR/.bashrc

cd $HOME

# See how we were called.
case "$1" in
 start)
  git reset --hard HEAD && git pull origin $BRANCH && ./gradlew :$APP_SERVER:clean
  ps -ef | grep $APP_SERVER | grep -v grep | awk '{print $2}' | xargs kill
  ./gradlew :$APP_SERVER:bootRun &
  echo $! > $PID
  ;;
 stop)
  ps -ef | grep $APP_SERVER | grep -v grep | awk '{print $2}' | xargs kill
  ;;
*)
 echo "Usage: $APP_NAME {start\|stop}";;
esac
exit 0
